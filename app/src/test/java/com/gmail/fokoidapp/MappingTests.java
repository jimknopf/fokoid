/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp;

import android.util.Log;

import com.gmail.fokoidapp.marks.Mapping;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Log.class)
public class MappingTests {

    private static final float EPSILON = 0.00001f;

    @Before
    public void mockLogging() {
        PowerMockito.mockStatic(Log.class);
    }

    @Test
    public void testSortedOrder() {
        float[] marks = new float[] {0.78f, 0.53f, 0.87f, 0.99f, 0.64f};
        Mapping mapping = new Mapping(marks, true, false);
        assertArrayEquals(new int[] {1, 4, 0, 2, 3}, mapping.getOrder());
    }

    @Test
    public void testUnsortedOrder() {
        float[] marks = new float[] {0.78f, 0.53f, 0.87f, 0.99f, 0.64f};
        Mapping mapping = new Mapping(marks, false, false);
        assertArrayEquals(new int[] {0, 1, 2, 3, 4}, mapping.getOrder());
    }

    @Test
    public void testHardStops() {
        float[] marks = new float[] {0.25f, 0.75f};
        Mapping mapping = new Mapping(marks, false, false);
        assertEquals(0.25f, mapping.mapPosition(0f), EPSILON);
        assertEquals(0.75f, mapping.mapPosition(1f), EPSILON);
    }

    @Test
    public void testHardStopsWithSmoothing() {
        float[] marks = new float[] {0.25f, 0.75f};
        Mapping mapping = new Mapping(marks, false, true);
        assertEquals(0.25f, mapping.mapPosition(0f), EPSILON);
        assertEquals(0.75f, mapping.mapPosition(1f), EPSILON);
    }

    @Test
    public void testFocusRange() {
        float[] marks = new float[] {0.59f, 0.21f, 0.22f, 0.78f, 0.16f};
        Mapping mapping = new Mapping(marks, true, false);
        for (float position = 0f; position < 1f; position += 0.01f) {
            float focus = mapping.mapPosition(position);
            assertTrue(focus >= 0.16f);
            assertTrue(focus <= 0.78f);
        }
    }

    @Test
    public void testFocusRangeWithSmoothing() {
        float[] marks = new float[] {0.59f, 0.21f, 0.22f, 0.78f, 0.16f};
        Mapping mapping = new Mapping(marks, true, true);
        for (float position = 0f; position < 1f; position += 0.01f) {
            float focus = mapping.mapPosition(position);
            assertTrue(focus >= 0.16f);
            assertTrue(focus <= 0.78f);
        }
    }

    @Test
    public void testConstantGap() {
        float[] marksA = new float[] {0.59f, 0.21f, 0.22f, 0.78f, 0.16f};
        float[] marksB = new float[] {0.25f, 0.5f, 0.75f};
        Mapping mappingA = new Mapping(marksA, false, true);
        Mapping mappingB = new Mapping(marksB, false, true);
        float[] positionsA = mappingA.getPositions();
        float[] positionsB = mappingB.getPositions();
        assertEquals(positionsA[0], positionsB[0], EPSILON);
        assertEquals(positionsA[positionsA.length - 1], positionsB[positionsB.length - 1], EPSILON);
    }

    @Test
    public void testUniquePositions() {
        float[] marks = new float[] {0.25f, 0.5f, 0.75f};
        Mapping mapping = new Mapping(marks, true, false);
        assertEquals(1, mapping.mapFocus(0.25f - EPSILON).length);
        assertEquals(1, mapping.mapFocus(0.25f).length);
        assertEquals(1, mapping.mapFocus(0.375f).length);
        assertEquals(1, mapping.mapFocus(0.5f).length);
        assertEquals(1, mapping.mapFocus(0.625f).length);
        assertEquals(1, mapping.mapFocus(0.75f).length);
        assertEquals(1, mapping.mapFocus(0.75f + EPSILON).length);
    }

    @Test
    public void testDuplicatePositions() {
        float[] marks = new float[] {0.4f, 0.6f, 0.2f};
        Mapping mapping = new Mapping(marks, false, false);
        assertEquals(2, mapping.mapFocus(0.4f - EPSILON).length);
        assertEquals(2, mapping.mapFocus(0.4f).length);
        assertEquals(2, mapping.mapFocus(0.5f).length);
        assertEquals(1, mapping.mapFocus(0.6f).length);
        assertEquals(2, mapping.mapFocus(0.5f + EPSILON).length);
        assertEquals(1, mapping.mapFocus(0.2f).length);
        assertEquals(1, mapping.mapFocus(0.2f + EPSILON).length);
    }

}
