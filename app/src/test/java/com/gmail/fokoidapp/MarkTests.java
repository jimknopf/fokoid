/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp;

import android.util.Log;

import com.gmail.fokoidapp.marks.Marks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Log.class)
public class MarkTests {

    private static final float EPSILON = 0.00001f;

    @Before
    public void mockLogging() {
        PowerMockito.mockStatic(Log.class);
    }

    @Before
    public void resetMarks() {
        Marks marks = Marks.get();
        while (!marks.isAtMinCapacity()) {
            marks.remove(0);
        }
        marks.update(0, 0f);
        marks.update(1, 1f);
    }

    @Test
    public void testAddDuplicateMark() {
        Marks marks = Marks.get();
        marks.update(0, 0.5f);
        assertFalse(marks.add(0.5f));
    }

    @Test
    public void testUpdateMarkWithDuplicate() {
        Marks marks = Marks.get();
        marks.update(0, 0.5f);
        assertFalse(marks.update(1, 0.5f));
    }

    @Test
    public void testMinimalUpdate() {
        Marks marks = Marks.get();
        marks.update(0, 0.5f);
        assertTrue(marks.update(0, 0.5f + EPSILON));
    }

    @Test
    public void testLowerLimit() {
        Marks marks = Marks.get();
        assertFalse(marks.remove(0));
        assertTrue(marks.isAtMinCapacity());
        assertEquals(2, marks.size());
        assertEquals(2, marks.getAll().length);
    }

    @Test
    public void testUpperLimit() {
        Marks marks = Marks.get();
        marks.add(0.2f);
        marks.add(0.4f);
        marks.add(0.6f);
        assertFalse(marks.add(0.8f));
        assertTrue(marks.isAtMaxCapacity());
        assertEquals(5, marks.size());
        assertEquals(5, marks.getAll().length);
    }

}
