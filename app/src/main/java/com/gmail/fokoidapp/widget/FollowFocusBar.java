/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.SeekBar;

import com.gmail.fokoidapp.R;
import com.gmail.fokoidapp.marks.Mapping;

/**
 * Seek bar with custom draw code to visualize the given {@link Mapping}.
 */
@SuppressLint("AppCompatCustomView")
public class FollowFocusBar extends SeekBar {

    /** The intrinsic widget height is {@value}dp. */
    private static final int HEIGHT = 128;

    /** Icons are rendered at size {@value}dp. */
    private static final int ICON_SIZE = 24;

    /** The visualization uses {@value}dp gaps in between elements. */
    private static final int GAP = 4;

    /** All elements are drawn with a stroke width of {@value}dp. */
    private static final int STROKE_WIDTH = 2;

    private final Drawable mFocusDrawable = new Drawable() {

        @Override
        public void draw(Canvas canvas) {
            Rect bounds = getBounds();
            float width = bounds.width();
            float barWidth = width - mIconSize;

            int savePoint = canvas.save();
            canvas.translate(bounds.left, bounds.top);

            // draw horizontal guide lines
            canvas.save();
            canvas.translate(mGap, 0.5f * mHeight - 0.5f * mIconSize - mGap);
            canvas.drawLine(0f, 0f, width - 2f * mGap, 0f, mPrimaryColor);
            canvas.translate(0, mIconSize + 2f * mGap);
            canvas.drawLine(0f, 0f, width - 2f * mGap, 0f, mAccentColor);
            canvas.restore();

            canvas.translate(mIconSize / 2f, 0.5f * mHeight);

            // draw mark icons and ticks
            float[] positions = mFocusMapping.getPositions();
            int[] ordering = mFocusMapping.getOrder();
            for (int i = 0; i < positions.length; i++) {
                float position = positions[i] * barWidth;
                canvas.save();
                canvas.translate(position, 0f);
                canvas.drawLine(0f, -0.5f * mIconSize - mGap, 0f, -mIconSize - mGap, mPrimaryColor);
                canvas.drawLine(0f, 0.5f * mIconSize + mGap, 0f, mIconSize + mGap, mAccentColor);
                canvas.translate(-0.5f * mIconSize, -0.5f * mIconSize);
                mMarkDrawables[ordering[i]].draw(canvas);
                canvas.restore();
            }

            // draw target focus points
            float progress = (float) getProgress() / getMax();
            float focus = mFocusMapping.mapPosition(progress);
            for (float targetPosition : mFocusMapping.mapFocus(focus)) {
                float position = targetPosition * barWidth;
                canvas.save();
                canvas.translate(position, 0.5f * mIconSize + mGap);
                canvas.drawLine(0f, 0f, 0f, mIconSize - mGap, mAccentColor);
                canvas.translate(-0.5f * mIconSize, mIconSize);
                mTargetDrawable.draw(canvas);
                canvas.restore();
            }

            // draw camera focus points
            for (float cameraPosition : mCameraPositions) {
                float position = cameraPosition * barWidth;
                canvas.save();
                canvas.translate(position, -0.5f * mIconSize - mGap);
                canvas.drawLine(0f, 0f, 0f, -mIconSize + mGap, mPrimaryColor);
                canvas.translate(-0.5f * mIconSize, -2f * mIconSize);
                mCameraDrawable.draw(canvas);
                canvas.restore();
            }

            canvas.restoreToCount(savePoint);
        }

        @Override
        public void setAlpha(int alpha) {
            // ignore
        }

        @Override
        public void setColorFilter(ColorFilter colorFilter) {
            // ignore
        }

        @Override
        public int getOpacity() {
            return PixelFormat.TRANSLUCENT;
        }

        @Override
        public int getIntrinsicHeight() {
            return mHeight;
        }
    };

    private final int mHeight;
    private final int mIconSize;
    private final int mGap;
    private final int mStrokeWidth;

    private final Paint mPrimaryColor;
    private final Paint mAccentColor;

    private static final int[] sMarkDrawableIds = new int[]{R.drawable.follow_focus_mark_1_24dp,
            R.drawable.follow_focus_mark_2_24dp, R.drawable.follow_focus_mark_3_24dp, R.drawable.follow_focus_mark_4_24dp,
            R.drawable.follow_focus_mark_5_24dp};

    private final Drawable[] mMarkDrawables;
    private final Drawable mTargetDrawable;
    private final Drawable mCameraDrawable;

    private Mapping mFocusMapping;
    private float[] mCameraPositions;

    public FollowFocusBar(Context context) {
        this(context, null);
    }

    public FollowFocusBar(Context context, AttributeSet attrs) {
        super(context, attrs);

        // The Android canvas uses physical (px) instead of device independent pixels (dp):
        // convert constants from dp to px.
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int dimension = TypedValue.COMPLEX_UNIT_DIP;
        mHeight = (int) TypedValue.applyDimension(dimension, HEIGHT, metrics);
        mIconSize = (int) TypedValue.applyDimension(dimension, ICON_SIZE, metrics);
        mGap = (int) TypedValue.applyDimension(dimension, GAP, metrics);
        mStrokeWidth = (int) TypedValue.applyDimension(dimension, STROKE_WIDTH, metrics);

        Paint basePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        basePaint.setStyle(Paint.Style.STROKE);
        basePaint.setStrokeWidth(mStrokeWidth);
        basePaint.setStrokeCap(Paint.Cap.ROUND);

        mPrimaryColor = new Paint(basePaint);
        mPrimaryColor.setColor(ContextCompat.getColor(context, R.color.colorPrimary));

        mAccentColor = new Paint(basePaint);
        mAccentColor.setColor(ContextCompat.getColor(context, R.color.colorAccent));

        mMarkDrawables = new Drawable[sMarkDrawableIds.length];
        for (int i = 0; i < sMarkDrawableIds.length; i++) {
            Drawable drawable = context.getDrawable(sMarkDrawableIds[i]);
            drawable.setBounds(0, 0, mIconSize, mIconSize);
            mMarkDrawables[i] = drawable;
        }

        mTargetDrawable = context.getDrawable(R.drawable.follow_focus_target_focus_24dp);
        mTargetDrawable.setBounds(0, 0, mIconSize, mIconSize);
        mCameraDrawable = context.getDrawable(R.drawable.follow_focus_camera_24dp);
        mCameraDrawable.setBounds(0, 0, mIconSize, mIconSize);

        mFocusMapping = new Mapping(new float[]{0.25f, 0.75f}, true, true);
        setCameraFocus(0.5f);

        setProgressDrawable(mFocusDrawable);
        setThumbOffset(-mIconSize / 2 + getThumbOffset());
    }

    /**
     * Sets the focus of the camera, i.e., the focus used to determine where to display the camera
     * indicator.
     *
     * @param focus the camera focus
     */
    public void setCameraFocus(float focus) {
        mCameraPositions = mFocusMapping.mapFocus(focus);
    }

    /**
     * Sets the {@link Mapping} to be used for the follow focus.
     *
     * @param mapping the mapping to use
     */
    public void setMapping(Mapping mapping) {
        mFocusMapping = mapping;
    }

}
