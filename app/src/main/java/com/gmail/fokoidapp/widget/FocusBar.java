/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.ProgressBar;

import com.gmail.fokoidapp.R;

/**
 * Visualization of the lens focus as horizontal bar. Overrides the {@link ProgressBar}'s default
 * {@link Drawable}s for it's horizontal style. Supports indeterminate states.
 */
public class FocusBar extends ProgressBar {

    /** The intrinsic widget height is {@value}dp. */
    private static final int HEIGHT = 24;

    /** The visualization uses {@value}dp gaps in between elements. */
    private static final int GAP = 4;

    /** All elements are drawn with a stroke width of {@value}dp. */
    private static final int STROKE_WIDTH = 2;

    private final Drawable focusDrawable = new Drawable() {
        @Override
        public void draw(Canvas canvas) {
            Rect bounds = getBounds();
            float width = bounds.width();
            float barWidth = width - 2f * (mHeight + mGap);

            int savePoint = canvas.save();
            canvas.translate(bounds.left, bounds.top);

            mTeleIcon.draw(canvas);

            canvas.save();
            canvas.translate(width - mHeight, 0f);
            mWideIcon.draw(canvas);
            canvas.restore();

            canvas.translate(mHeight + mGap, 0f);
            canvas.drawLine(0f, mHeight / 2f, barWidth, mHeight / 2f, mPrimaryColor);
            if (!isIndeterminate()) {
                float progress = (float) getProgress() / getMax();
                float position = barWidth * progress;
                canvas.drawLine(position, mGap, position, mHeight - mGap, mAccentColor);
            }

            canvas.restoreToCount(savePoint);
        }

        @Override
        public void setAlpha(int alpha) {
            // ignore
        }

        @Override
        public void setColorFilter(ColorFilter colorFilter) {
            // ignore
        }

        @Override
        public int getOpacity() {
            return PixelFormat.TRANSLUCENT;
        }

        @Override
        public int getIntrinsicHeight() {
            return mHeight;
        }
    };

    private final Paint mPrimaryColor;
    private final Paint mAccentColor;

    private final Drawable mTeleIcon;
    private final Drawable mWideIcon;

    private final int mHeight;
    private final int mGap;
    private final int mStrokeWidth;

    public FocusBar(Context context) {
        this(context, null);
    }

    public FocusBar(Context context, AttributeSet attrs) {
        super(context, attrs);

        // The Android canvas uses physical (px) instead of device independent pixels (dp):
        // convert constants from dp to px.
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int dimension = TypedValue.COMPLEX_UNIT_DIP;
        mHeight = (int) TypedValue.applyDimension(dimension, HEIGHT, metrics);
        mGap = (int) TypedValue.applyDimension(dimension, GAP, metrics);
        mStrokeWidth = (int) TypedValue.applyDimension(dimension, STROKE_WIDTH, metrics);

        Paint basePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        basePaint.setStyle(Paint.Style.STROKE);
        basePaint.setStrokeWidth(mStrokeWidth);
        basePaint.setStrokeCap(Paint.Cap.ROUND);

        mPrimaryColor = new Paint(basePaint);
        mPrimaryColor.setColor(ContextCompat.getColor(context, R.color.colorPrimary));

        mAccentColor = new Paint(basePaint);
        mAccentColor.setColor(ContextCompat.getColor(context, R.color.colorAccent));

        mTeleIcon = context.getDrawable(R.drawable.focus_tele_24dp);
        mTeleIcon.setBounds(0, 0, mHeight, mHeight);

        mWideIcon = context.getDrawable(R.drawable.focus_wide_24dp);
        mWideIcon.setBounds(0, 0, mHeight, mHeight);

        setProgressDrawable(focusDrawable);
        setIndeterminateDrawable(focusDrawable);
    }

}
