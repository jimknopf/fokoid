/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import java.util.Set;

import com.gmail.fokoidapp.camera.CameraController;
import com.gmail.fokoidapp.camera.CameraController.CameraControllerBinder;
import com.gmail.fokoidapp.camera.CameraState;
import com.gmail.fokoidapp.camera.CameraState.Command;
import com.gmail.fokoidapp.camera.CameraState.Flag;
import com.gmail.fokoidapp.camera.CameraState.StatusCallback;
import com.gmail.fokoidapp.camera.Lens;
import com.gmail.fokoidapp.marks.Mapping;
import com.gmail.fokoidapp.marks.Marks;
import com.gmail.fokoidapp.widget.FollowFocusBar;

/**
 * The follow focus activity. Provides a virtual follow focus visualized by a single
 * {@link FollowFocusBar} and a quick navigation button for each mark.
 */
public class FollowFocus extends Activity {

    /**
     * Callback for camera status changes and lens updates.
     */
    private final StatusCallback mStatusCallback = new StatusCallback() {

        @Override
        public void onFlagUpdate(Set<CameraState.Flag> flags) {
            final boolean ready = flags.contains(Flag.CONNECTED) && flags.contains(Flag.CALIBRATED);
            final boolean busy = flags.contains(Flag.CONNECTING)
                    || flags.contains(Flag.CALIBRATING);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mCameraToggle.setEnabled(ready && !busy);
                    if (mCameraToggle.isChecked()) {
                        mCameraToggle.setChecked(ready && !busy);
                    }
                    mSyncButton.setEnabled(ready && !busy && !mCameraToggle.isChecked());
                }
            });
        }

        @Override
        public void onLensUpdate(Lens lens) {
            updateCamera(lens);
            mFollowFocus.postInvalidate();
        }

    };

    /**
     * Delegates user interaction with the follow focus to the camera controller. Keeps track of the
     * direction of the focus changes to differentiate between changes that continue a previous
     * sequence of changes in the same direction and new gestures (e.g., a change of direction).
     */
    private final OnSeekBarChangeListener mFocusListener = new OnSeekBarChangeListener() {

        // keep track of progress and direction (-1 = left, 0  = none, 1 = right)
        private int mPreviousProgress = 0;
        private int mPreviousDirection = 0;

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            int currentDirection = progress - mPreviousProgress < 0 ? -1 : 1;
            boolean continuous = currentDirection == mPreviousDirection;

            mPreviousProgress = progress;
            mPreviousDirection = currentDirection;

            if (mCameraToggle.isChecked() && mCameraController != null) {
                float focus = mMapping.mapPosition((float) progress / seekBar.getMax());
                mCameraController.setFocus(focus, mFastStepsToggle.isChecked(), continuous);
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            mPreviousProgress = mFollowFocus.getProgress();
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            mPreviousDirection = 0;
        }

    };

    /** Connection to {@link CameraController} service. */
    private final ServiceConnection mCameraControllerConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            CameraControllerBinder binder = (CameraControllerBinder) service;
            mCameraController = binder.get();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mCameraController = null;
        }
    };

    private final static int[] sMarkButtonIds = new int[]{R.id.gotoMark1, R.id.gotoMark2,
            R.id.gotoMark3, R.id.gotoMark4, R.id.gotoMark5};

    private CameraState mCameraState;
    private CameraController mCameraController;

    private Marks mMarks;
    private Mapping mMapping;

    private CheckBox mSortingToggle;
    private CheckBox mSmoothingToggle;
    private CheckBox mFastStepsToggle;
    private CheckBox mCameraToggle;
    private ImageButton mSyncButton;
    private ImageButton[] mMarkButtons;
    private FollowFocusBar mFollowFocus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_focus);

        mSortingToggle = (CheckBox) findViewById(R.id.checkSorting);
        mSmoothingToggle = (CheckBox) findViewById(R.id.checkSmoothing);
        mFastStepsToggle = (CheckBox) findViewById(R.id.checkFastSteps);
        mCameraToggle = (CheckBox) findViewById(R.id.cameraConnection);

        mSyncButton = (ImageButton) findViewById(R.id.syncFocus);

        mFollowFocus = (FollowFocusBar) findViewById(R.id.followFocus);

        mMarkButtons = new ImageButton[Marks.MAX_MARKS];
        for (int i = 0; i < Marks.MAX_MARKS; i++) {
            mMarkButtons[i] = (ImageButton) findViewById(sMarkButtonIds[i]);
        }

        OnCheckedChangeListener updater = new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateMapping();
            }
        };
        mSortingToggle.setOnCheckedChangeListener(updater);
        mSmoothingToggle.setOnCheckedChangeListener(updater);

        mCameraToggle.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mSortingToggle.setEnabled(!isChecked);
                mSmoothingToggle.setEnabled(!isChecked);
                mFastStepsToggle.setEnabled(!isChecked);
                if (isChecked) {
                    // fire last focus change event again
                    mFocusListener.onProgressChanged(mFollowFocus,
                            mFollowFocus.getProgress(), true);
                }
            }
        });

        mFollowFocus.setOnSeekBarChangeListener(mFocusListener);

        mMarks = Marks.get();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_follow_focus, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_marks) {
            Intent marks = new Intent(this, MarkList.class);
            startActivity(marks);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        updateMapping();

        for (int i = 0; i < mMarks.size(); i++) {
            mMarkButtons[i].setEnabled(true);
            mMarkButtons[i].setVisibility(View.VISIBLE);
        }

        Intent bindController = new Intent(this, CameraController.class);
        bindService(bindController, mCameraControllerConnection, BIND_AUTO_CREATE);

        mCameraState = CameraState.get(getApplicationContext());
        mCameraState.registerCallback(mStatusCallback, true);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mCameraState.unregisterCallback(mStatusCallback);
        unbindService(mCameraControllerConnection);
    }

    public void onGotoMark(View view) {
        int index;
        switch(view.getId()) {
            case R.id.gotoMark5:
                index = 4;
                break;
            case R.id.gotoMark4:
                index = 3;
                break;
            case R.id.gotoMark3:
                index = 2;
                break;
            case R.id.gotoMark2:
                index = 1;
                break;
            default:
                index = 0;
        }
        index = mMapping.getOrder()[index];
        int progress = (int) (mMapping.getPositions()[index] * mFollowFocus.getMax());
        mFollowFocus.setProgress(progress);
        mFollowFocus.postInvalidate();
    }

    public void onSyncFocus(View view) {
        if (mCameraController != null) {
            mCameraController.syncFocus();
        }
    }

    private void updateCamera(Lens lens) {
        if (lens.isIdentified()) {
            float focus = (float) lens.getFocusPoint() / lens.getFocusRange();
            mFollowFocus.setCameraFocus(focus);
            mFollowFocus.setMax(lens.getFocusRange());
        }
    }

    private void updateMapping() {
        mMapping = new Mapping(mMarks.getAll(), mSortingToggle.isChecked(),
                mSmoothingToggle.isChecked());
        mFollowFocus.setMapping(mMapping);
        if (mCameraState != null) {
            updateCamera(mCameraState.getLens());
        }
        mFollowFocus.postInvalidate();
    }

}
