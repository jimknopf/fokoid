/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.gmail.fokoidapp.marks.Marks;
import com.gmail.fokoidapp.widget.FocusBar;

/**
 * Activity to manage up to {@value Marks#MAX_MARKS} marks. Allows to add, edit, remove (swipe) and
 * rearrange (drag and drop) marks.
 */
public class MarkList extends Activity {

    private static final int CODE_CREATE_MARK = 1;
    private static final int CODE_EDIT_MARK = 2;

    /**
     * Wrapper class for a view of a single mark. Allows for accessing the view's icon and focus
     * visualization.
     */
    private class MarkHolder extends RecyclerView.ViewHolder {

        final ImageView mMarkIcon;
        final FocusBar mFocusBar;

        MarkHolder(View itemView) {
            super(itemView);
            mMarkIcon = (ImageView) itemView.findViewById(R.id.markIcon);
            mFocusBar = (FocusBar) itemView.findViewById(R.id.markFocus);

            ImageButton edit = (ImageButton) itemView.findViewById(R.id.editMark);
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEditMark(getAdapterPosition());
                }
            });

            ImageButton delete = (ImageButton) itemView.findViewById(R.id.deleteMark);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onDeleteMark(getAdapterPosition());
                }
            });

            ImageView drag = (ImageView) itemView.findViewById(R.id.dragMark);
            drag.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getActionMasked() == MotionEvent.ACTION_DOWN
                            && mTouchHelper != null) {
                        mTouchHelper.startDrag(MarkHolder.this);
                    }
                    return false;
                }
            });
        }
    }

    /**
     * Binds the view of a single mark to its counterpart managed by the {@link Marks} singleton.
     */
    private final RecyclerView.Adapter<MarkHolder> mAdapter =
            new RecyclerView.Adapter<MarkHolder>() {

        @Override
        public MarkHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            return new MarkHolder(inflater.inflate(R.layout.content_mark, parent, false));
        }

        @Override
        public void onBindViewHolder(MarkHolder holder, int position) {
            holder.mMarkIcon.setImageDrawable(mMarkDrawables[position]);
            FocusBar focusBar = holder.mFocusBar;
            focusBar.setProgress((int) (focusBar.getMax() * mMarks.get(position)));
        }

        @Override
        public int getItemCount() {
            return mMarks.size();
        }

    };

    /**
     * Handles touch gestures and indicates which gestures are currently enabled. Delays propagation
     * of changes due to a drag and drop gesture until the user drops the item. This prevents the
     * update of the items (i.e., change of mark icon and focus) during the gesture. In particular,
     * the content of the dragged item remains the same until dropped.
     */
    private final ItemTouchHelper.Callback mTouchCallback = new ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP | ItemTouchHelper.DOWN,
            ItemTouchHelper.END) {

        // Track position of dragged items: -1 indicates the position is unknown.
        private int mMoveFrom = -1;
        private int mMoveTo = -1;

        @Override
        public boolean isItemViewSwipeEnabled() {
            return !mMarks.isAtMinCapacity();
        }

        @Override
        public boolean isLongPressDragEnabled() {
            return true;
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                              RecyclerView.ViewHolder target) {
            int from = viewHolder.getAdapterPosition();
            int to = target.getAdapterPosition();

            if (mMoveFrom == -1) {
                mMoveFrom = from;
            }
            mMoveTo = to;

            mMarks.swap(from, to);
            mAdapter.notifyItemMoved(from, to);
            return true;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int index = viewHolder.getAdapterPosition();
            if (mMarks.remove(index)) {
                mAdapter.notifyItemRemoved(index);
                mAdapter.notifyItemRangeChanged(index, mMarks.size() - index);
            }
        }

        @Override
        public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            super.clearView(recyclerView, viewHolder);
            if (mMoveFrom > -1 && mMoveTo > -1 && mMoveFrom != mMoveTo) {
                mAdapter.notifyItemRangeChanged(Math.min(mMoveFrom, mMoveTo),
                        Math.abs(mMoveFrom - mMoveTo) + 1);
            }
            mMoveFrom = -1;
            mMoveTo = -1;
        }
    };

    private static final int[] sMarkDrawableIds = new int[]{R.drawable.action_goto_mark_1_24dp,
            R.drawable.action_goto_mark_2_24dp, R.drawable.action_goto_mark_3_24dp,
            R.drawable.action_goto_mark_4_24dp, R.drawable.action_goto_mark_5_24dp};

    private Drawable[] mMarkDrawables;
    private Marks mMarks;
    private ItemTouchHelper mTouchHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marks);

        mMarkDrawables = new Drawable[sMarkDrawableIds.length];
        for (int i = 0; i < sMarkDrawableIds.length; i++) {
            Drawable drawable = getDrawable(sMarkDrawableIds[i]);
            mMarkDrawables[i] = drawable;
        }

        RecyclerView list = (RecyclerView) findViewById(R.id.markList);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(mAdapter);
        mTouchHelper = new ItemTouchHelper(mTouchCallback);
        mTouchHelper.attachToRecyclerView(list);

        mMarks = Marks.get();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_marks, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean consumed = false;
        switch (item.getItemId()) {
            case R.id.action_add_mark:
                onAddMark();
                consumed = true;
                break;
            case R.id.action_follow_focus:
                Intent followFocus = new Intent(this, FollowFocus.class);
                startActivity(followFocus);
                consumed = true;
                break;
        }
        return consumed || super.onOptionsItemSelected(item);
    }

    public void onAddMark() {
        if (mMarks.isAtMaxCapacity()) {
            Toast.makeText(this, R.string.marks_at_maximum, Toast.LENGTH_LONG).show();
        } else {
            Intent addMark = new Intent(this, MarkEditor.class);
            startActivityForResult(addMark, CODE_CREATE_MARK);
        }
    }

    private void onEditMark(int index) {
        Intent editMark = new Intent(this, MarkEditor.class);
        editMark.putExtra(MarkEditor.KEY_INDEX, index);
        editMark.putExtra(MarkEditor.KEY_MARK, mMarks.get(index));
        startActivityForResult(editMark, CODE_EDIT_MARK);
    }

    private void onDeleteMark(int index) {
        if (mMarks.remove(index)) {
            mAdapter.notifyItemRemoved(index);
        } else {
            Toast.makeText(this, R.string.marks_at_minimum, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CODE_CREATE_MARK) {
                float mark = data.getFloatExtra(MarkEditor.KEY_MARK, 0f);
                if (mMarks.add(mark)) {
                    mAdapter.notifyItemInserted(mMarks.size() - 1);
                } else {
                    Toast.makeText(this, R.string.marks_add_duplicate, Toast.LENGTH_LONG).show();
                }
            } else if (requestCode == CODE_EDIT_MARK) {
                int index = data.getIntExtra(MarkEditor.KEY_INDEX, 0);
                float mark = data.getFloatExtra(MarkEditor.KEY_MARK, 0f);
                if (mMarks.update(index, mark)) {
                    mAdapter.notifyItemChanged(index);
                } else {
                    Toast.makeText(this, R.string.marks_edit_duplicate, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

}
