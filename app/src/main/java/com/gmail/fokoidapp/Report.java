/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gmail.fokoidapp.camera.CameraController;
import com.gmail.fokoidapp.camera.CameraController.CameraControllerBinder;
import com.gmail.fokoidapp.camera.CameraController.ReportCallback;
import com.gmail.fokoidapp.camera.CameraState;
import com.gmail.fokoidapp.camera.CameraState.Flag;
import com.gmail.fokoidapp.camera.CameraState.StatusCallback;
import com.gmail.fokoidapp.camera.Lens;

import java.util.Set;

/**
 * Activity to create a report of the communication (attempts) with the camera.
 */
public class Report extends Activity {

    /**
     * Callback for camera status changes. En- or disables report button.
     */
    private final StatusCallback mStatusCallback = new StatusCallback() {

        @Override
        public void onFlagUpdate(Set<Flag> flags) {
            final boolean ready = flags.contains(Flag.ONLINE)
                    && !(flags.contains(Flag.CONNECTING) || flags.contains(Flag.CALIBRATING));
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    mReportButton.setEnabled(ready);
                    mCopyButton.setEnabled(ready && mReport != null);
                    mMailButton.setEnabled(ready && mReport != null);
                }

            });
        }

        @Override
        public void onLensUpdate(Lens lens) {
            // ignore
        }
    };

    /**
     * Connection to {@link CameraController} service.
     */
    private final ServiceConnection mCameraControllerConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            CameraControllerBinder binder = (CameraControllerBinder) service;
            mCameraController = binder.get();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mCameraController = null;
        }
    };

    private CameraState mCameraState;
    private CameraController mCameraController;

    private ProgressBar mReportProgress;
    private Button mReportButton;
    private Button mCopyButton;
    private Button mMailButton;

    private String mReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        mReportProgress = (ProgressBar) findViewById(R.id.reportProgress);
        mReportButton = (Button) findViewById(R.id.createReport);
        mCopyButton = (Button) findViewById(R.id.copyReport);
        mMailButton = (Button) findViewById(R.id.mailReport);

        mCameraState = CameraState.get(getApplicationContext());
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent bindController = new Intent(this, CameraController.class);
        bindService(bindController, mCameraControllerConnection, BIND_AUTO_CREATE);
        mCameraState.registerCallback(mStatusCallback, true);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(mCameraControllerConnection);
        mCameraState.unregisterCallback(mStatusCallback);
    }

    public void onCreateReport(View view) {
        if (mCameraController != null) {
            mReportProgress.setVisibility(View.VISIBLE);
            mCameraController.createReport(new ReportCallback() {

                @Override
                public void onFinish(final String report) {
                    mReport = report;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mReportProgress.setVisibility(View.INVISIBLE);
                            mCopyButton.setEnabled(true);
                            mMailButton.setEnabled(true);
                        }
                    });
                }

            });
        }
    }

    public void onCopy(View view) {
        if (mReport != null) {
            ClipboardManager manager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData data = ClipData.newPlainText(getString(R.string.report_report_subject),
                    mReport);
            manager.setPrimaryClip(data);

            Toast toast = Toast.makeText(this, R.string.report_copied_report, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    public void onMail(View view) {
        if (mReport != null) {
            String emailUri = "mailto:" + getString(R.string.report_report_address) +
                    "?subject=" + Uri.encode(getString(R.string.report_report_subject)) +
                    "&body=" + Uri.encode(mReport);
            Intent sendEmail = new Intent(Intent.ACTION_SENDTO);
            sendEmail.setData(Uri.parse(emailUri));
            try {
                startActivity(sendEmail);
            } catch (ActivityNotFoundException e) {
                Toast toast = Toast.makeText(this, R.string.report_no_email_app, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }

}