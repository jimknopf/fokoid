/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp.camera;

import java.util.Locale;

/**
 * Encapsulates information about the connected lens.
 */
public class Lens {

    private boolean mIdentified;

    private int mMinFocalLength;
    private int mMaxFocalLength;
    private int mFocusRange;
    private int mTeleRange;

    private boolean mCalibrated;

    private float mTeleStepSize;
    private float mTeleFastStepSize;
    private float mWideStepSize;
    private float mWideFastStepSize;

    private int mFocusPoint;

    Lens() {
        mIdentified = false;
        mCalibrated = false;
    }

    Lens(Lens other) {
        mIdentified = other.isIdentified();

        mMinFocalLength = other.getMinFocalLength();
        mMaxFocalLength = other.getMaxFocalLength();
        mFocusRange = other.getFocusRange();
        mTeleRange = other.getTeleRange();

        mCalibrated = other.isCalibrated();

        mTeleStepSize = other.getTeleStepSize();
        mTeleFastStepSize = other.getTeleFastStepSize();
        mWideStepSize = other.getWideStepSize();
        mWideFastStepSize = other.getWideFastStepSize();

        mFocusPoint = other.getFocusPoint();
    }

    /**
     * @return {@code true} iff the lens was identified
     */
    public boolean isIdentified() {
        return mIdentified;
    }

    /**
     * @return the minimum focal length in mm
     */
    public int getMinFocalLength() {
        return mMinFocalLength;
    }

    /**
     * @return the maximum focal length in mm
     */
    public int getMaxFocalLength() {
        return mMaxFocalLength;
    }

    /**
     * Returns the total focus range, i.e., the maximum value the focus point can be set to.
     *
     * @return the focus range (logical unit)
     */
    public int getFocusRange() {
        return mFocusRange;
    }

    /**
     * Returns the tele focus range. The setp size differs in this section of the total focus range.
     *
     * @return the tele range (logical unit)
     */
    public int getTeleRange() {
        return mTeleRange;
    }

    /**
     * @return {@code true} if step sizes are available
     */
    public boolean isCalibrated() {
        return mCalibrated;
    }

    /**
     * @return mean step size in the tele range
     */
    public float getTeleStepSize() {
        return mTeleStepSize;
    }

    /**
     * @return mean fast step size in the tele range
     */
    public float getTeleFastStepSize() {
        return mTeleFastStepSize;
    }

    /**
     * @return mean step size outside of the tele range
     */
    public float getWideStepSize() {
        return mWideStepSize;
    }

    /**
     * @return mean fast step size outside of the tele range
     */
    public float getWideFastStepSize() {
        return mWideFastStepSize;
    }

    /**
     * @return mean step size for the current focus position
     */
    public float getCurrentStepSize() {
        return mFocusPoint < mTeleRange ? mTeleStepSize : mWideStepSize;
    }

    /**
     * @return mean fast step size for the current focus position
     */
    public float getCurrentFastStepSize() {
        return mFocusPoint < mTeleRange ? mTeleFastStepSize : mWideFastStepSize;
    }

    /**
     * @return the last known focus point of the camera
     */
    public int getFocusPoint() {
        return mFocusPoint;
    }

    void setIdentified(boolean identified) {
        this.mIdentified = identified;
    }

    void setMinFocalLength(int minFocalLenght) {
        this.mMinFocalLength = minFocalLenght;
    }

    void setMaxFocalLength(int maxFocalLength) {
        this.mMaxFocalLength = maxFocalLength;
    }

    void setFocusRange(int focusRange) {
        this.mFocusRange = focusRange;
    }

    void setTeleMark(int teleMark) {
        this.mTeleRange = teleMark;
    }

    void setCalibrated(boolean calibrated) {
        this.mCalibrated = calibrated;
    }

    void setTeleStepSize(float teleStepSize) {
        this.mTeleStepSize = teleStepSize;
    }

    void setTeleFastStepSize(float teleFastStepSize) {
        this.mTeleFastStepSize = teleFastStepSize;
    }

    void setWideStepSize(float wideStepSize) {
        this.mWideStepSize = wideStepSize;
    }

    void setWideFastStepSize(float wideFastStepSize) {
        this.mWideFastStepSize = wideFastStepSize;
    }

    void setFocusPoint(int focusPoint) {
        this.mFocusPoint = focusPoint;
    }

    @Override
    public String toString() {
        if (mIdentified) {
            if (mCalibrated) {
                return String.format(Locale.US, "%dmm - %dmm lens (range = %d, tele range = %d," +
                                " steps = [%.1f, %.1f, %.1f, %.1f])", mMinFocalLength,
                        mMaxFocalLength, mFocusRange, mTeleRange, mTeleStepSize, mTeleFastStepSize,
                        mWideStepSize, mWideFastStepSize);
            } else {
                return String.format(Locale.US, "%dmm - %dmm lens (range = %d, , tele range = %d)",
                        mMinFocalLength, mMaxFocalLength, mFocusRange, mTeleRange);
            }
        } else {
            return "Unidentified lens";
        }
    }
}
