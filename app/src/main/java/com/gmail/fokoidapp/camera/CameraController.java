/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp.camera;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Network;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.gmail.fokoidapp.R;
import com.gmail.fokoidapp.camera.CameraState.Flag;
import com.gmail.fokoidapp.camera.CameraState.Command;
import com.gmail.fokoidapp.camera.Response.Type;

/**
 * Service to connect to and control a camera.
 */
public class CameraController extends Service {

    /**
     * Callback for asynchronous report generation. See {@link
     * CameraController#createReport(ReportCallback)} for usage.
     */
    public interface ReportCallback {

        /**
         * @param report the final report
         */
        void onFinish(String report);

    }

    private static final String TAG = "CameraController";

    /** Stop polling thread after {@value} consecutive failed requests. */
    private static final int MAX_FAILED_REQUESTS = 3;

    /** Poll camera every {@value}ms. */
    private static final int POLLING_FREQUENCY = 5000;

    /** Poll camera every {@value}ms when pairing. */
    private static final int PAIRING_POLLING_FREQUENCY = 2000;

    /** Wait for {@value}ms between commands when creating a report. */
    private static final int REPORT_COOLDOWN = 2500;

    /** Poll camera up to {@value} times when pairing. */
    private static final int MAX_PAIRING_ATTEMPTS = 15;

    /** Wait for {@value}ms after auto focus. */
    private static final int AUTO_FOCUS_COOL_DOWN = 750;

    /** Take up to {@value} samples for each step type and range. */
    private static final int MAX_SAMPLES = 10;

    /** Semaphore to prevent launching of more than one polling thread. */
    private static final Semaphore POLLING_SEMAPHORE = new Semaphore(1);

    /** Wait for up to {@value}ms for a permit to start a new polling thread. */
    public static final int POLLING_PERMIT_TIMEOUT = 500;

    /** Sentinel for polling threads: if {@code false}, polling threads should stop. */
    private static final AtomicBoolean POLLING_SENTINEL = new AtomicBoolean(false);

    /** Available options for focus changes. */
    private enum FocusDirection {
        TELE,
        TELE_FAST,
        WIDE,
        WIDE_FAST,
        NONE
    }

    /** The global camera status. */
    private CameraState mCameraState;

    /** Thread pool for http commands. */
    private ExecutorService mCommandThread;

    /** Separate thread for periodic updates. */
    private Thread mPollingThread;

    /** Lock for changes of the last known focus position etc. */
    private Lock mFocusLock = new ReentrantLock();

    /** Ensure that at most one focus change task is scheduled/active at any time. */
    private Semaphore mFocusSemaphore = new Semaphore(1);

    /** The last know target focus point. */
    private float mTargetFocus = 0f;

    /** The last known camera focus point. */
    private float mCameraFocus = 0f;

    /**
     * The last known direction of the user input, e.g., whether the user dragged a focus slider
     * towards a mark closer to the camera than the last known camera focus (WIDE) or farther away
     * (TELE). The value NONE indicates that no such information is available.
     */
    private FocusDirection mInputDirection = FocusDirection.NONE;

    /**
     * Whether the target focus is closer to (WIDE) or farther away from (TELE) the last known
     * camera focus point.
     */
    private FocusDirection mTargetDirection = FocusDirection.NONE;

    /**
     * Indicates whether the camera passed the target focus with (one of) the last focus changes.
     * For example, if the current camera focus is 0.45 and the user requests a change to 0.5, the
     * controller might issue three commands changing the camera focus to 0.47, 0.49, and 0.51. This
     * last change to 0.51 passes the target focus.
     */
    private boolean mPassedTarget = false;

    /**
     * Binder that implements a simple getter for the {@link CameraController}.
     */
    public class CameraControllerBinder extends Binder {
        public CameraController get() {
            return CameraController.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Creating camera control service");

        mCameraState = CameraState.get(getApplicationContext());

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String ip = preferences.getString(getString(R.string.settings_camera_ip_key),
                getString(R.string.settings_camera_ip_default));
        Requests.setCameraIp(ip);

        // background thread to handle http commands
        mCommandThread = Executors.newSingleThreadExecutor();

        // (re)start polling thread
        if (mCameraState.getFlags().contains(Flag.CONNECTED)) {
            startPollingThread();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new CameraControllerBinder();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Destroying camera control service");
        mCommandThread.shutdown();
        mCommandThread = null;
        stopPollingThread();
    }

    /**
     * Attempts to (re)connect to the camera. In addition to the initial handshake, this method also
     * looks up camera capabilities and lens information.
     */
    public void connect() {
        final Network network = mCameraState.getNetwork();
        if (network == null) {
            Log.d(TAG, "Aborting connection (no network)");
            return;
        }
        mCameraState.setFlag(Flag.CONNECTING);
        mCommandThread.submit(new Runnable() {
            @Override
            public void run() {
                // initial handshake
                UUID uuid = UUID.randomUUID();
                Response response = Requests.requestAccess(network, uuid);
                boolean ok = readAccessRequest(response);

                // Older cameras do not require to request access and/or setting the device. In that
                // case, skip these requests and directly ask to turn on recording mode.
                if (isUnsupported(response)) {
                    // continue with requesting the recording mode
                    ok = true;
                } else {
                    // check for ongoing pairing
                    int pairingAttempts = 0;
                    while (!ok && CsvResponse.VALUE_UNDER_RESEARCH.equals(response.code())
                            && pairingAttempts < MAX_PAIRING_ATTEMPTS) {
                        try {
                            Thread.sleep(PAIRING_POLLING_FREQUENCY);
                        } catch (InterruptedException e) {
                            Log.d(TAG, "Polling for control interrupted", e);
                        }
                        response = Requests.requestAccess(network, uuid);
                        ok = readAccessRequest(response);
                        pairingAttempts++;
                    }

                    // try to set device name, ignore if unsupported
                    if (ok) {
                        response = Requests.setDeviceName(network);
                        ok = response.ok() || isUnsupported(response);
                    }
                }

                if (!ok) {
                    showToast(R.string.camera_ctrl_connect_handshake_failed);
                    mCameraState.unsetFlags(EnumSet.of(Flag.CONNECTED, Flag.CONNECTING));
                    return;
                }

                // try to change to recording mode
                response = Requests.changeToRecordMode(network);
                if (!response.ok()) {
                    showToast(R.string.camera_ctrl_connect_rec_mode_failed);
                    mCameraState.unsetFlags(EnumSet.of(Flag.CONNECTED, Flag.CONNECTING));
                    return;
                }

                // look up lens information
                response = Requests.getLensInfo(network);
                ok = readLensInfo(response);
                if (!ok) {
                    showToast(R.string.camera_ctrl_connect_lens_info_failed);
                    mCameraState.unsetFlags(EnumSet.of(Flag.CONNECTED, Flag.CONNECTING));
                    mCameraState.unsetLensInfo();
                    return;
                }

                // we have a connection
                mCameraState.setFlag(Flag.CONNECTED);
                startPollingThread();
                mCameraState.unsetFlag(Flag.CONNECTING);
            }
        });
    }

    /**
     * Attempts to calibrate the follow focus, i.e., determine step sizes for the current lens and
     * zoom level.
     */
    public void calibrate() {
        final Network network = mCameraState.getNetwork();
        if (network == null) {
            Log.d(TAG, "Aborting calibration (no network)");
            return;
        }

        final Lens lens = mCameraState.getLens();
        if (!lens.isIdentified()) {
            Log.d(TAG, "Aborting calibration (unidentified lens)");
            return;
        }

        mCameraState.setFlag(Flag.CALIBRATING);
        mCommandThread.submit(new Runnable() {
            @Override
            public void run() {
                int range = lens.getFocusRange();
                int teleRange = lens.getTeleRange();

                try {
                    // got to max tele value
                    int focusPointA, focusPointB;
                    do {
                        focusPointA = changeFocusOrThrow(FocusDirection.TELE_FAST);
                    } while (focusPointA > 0);

                    // sample normal step size in tele range
                    int sum = 0;
                    int count = 0;
                    boolean inTargetRange;
                    do {
                        focusPointB = changeFocusOrThrow(FocusDirection.WIDE);
                        inTargetRange = focusPointB < teleRange;
                        if (inTargetRange) {
                            sum += focusPointB - focusPointA;
                            count++;
                        }
                        focusPointA = focusPointB;
                    } while (inTargetRange && count < MAX_SAMPLES);

                    float teleStepSize = (float) sum / count;

                    // go back to max tele value
                    do {
                        focusPointA = changeFocusOrThrow(FocusDirection.TELE_FAST);
                    } while (focusPointA > 0);

                    // sample fast step size in tele range
                    sum = 0;
                    count = 0;
                    do {
                        focusPointB = changeFocusOrThrow(FocusDirection.WIDE_FAST);
                        inTargetRange = focusPointB < teleRange;
                        if (inTargetRange) {
                            sum += focusPointB - focusPointA;
                            count++;
                        }
                        focusPointA = focusPointB;
                    } while (inTargetRange && count < MAX_SAMPLES);

                    float teleFastStepSize = (float) sum / count;

                    // go to max wide value
                    do {
                        focusPointA = changeFocusOrThrow(FocusDirection.WIDE_FAST);
                    } while (focusPointA < range);

                    // sample step size in wide range
                    sum = 0;
                    count = 0;
                    do {
                        focusPointB = changeFocusOrThrow(FocusDirection.TELE);
                        inTargetRange = focusPointB > teleRange;
                        if (inTargetRange) {
                            sum += focusPointA - focusPointB;
                            count++;
                        }
                        focusPointA = focusPointB;
                    } while (inTargetRange && count < MAX_SAMPLES);

                    float wideStepSize = (float) sum / count;

                    // go back to max wide value
                    do {
                        focusPointA = changeFocusOrThrow(FocusDirection.WIDE_FAST);
                    } while (focusPointA < range);

                    // sample fast step size in wide range
                    sum = 0;
                    count = 0;
                    do {
                        focusPointB = changeFocusOrThrow(FocusDirection.TELE_FAST);
                        inTargetRange = focusPointB > teleRange;
                        if (inTargetRange) {
                            sum += focusPointA - focusPointB;
                            count++;
                        }
                        focusPointA = focusPointB;
                    } while (inTargetRange && count < MAX_SAMPLES);

                    float wideFastStepSize = (float) sum / count;

                    // calibration succeeded...
                    mCameraState.setLensCalibration(teleStepSize, teleFastStepSize, wideStepSize,
                            wideFastStepSize);
                } catch (IOException e) {
                    showToast(R.string.camera_ctrl_calibration_failed);
                } finally {
                    mCameraState.unsetFlag(Flag.CALIBRATING);
                }
            }
        });
    }

    /**
     * Polls the camera focus by moving the focus point nearer to the camera and back.
     */
    public void syncFocus() {
        final Network network = mCameraState.getNetwork();
        if (network == null) {
            Log.d(TAG, "Aborting focus sync (no network)");
            return;
        }
        mCommandThread.submit(new Runnable() {
            @Override
            public void run() {
                Response response = Requests.focusWide(network);
                if (response.ok()) {
                    response = Requests.focusTele(network);
                }
                if (response.ok()) {
                    readFocus(response);
                } else {
                    showToast(R.string.camera_ctrl_sync_failed);
                }
            }
        });
    }

    /**
     * Triggers the auto focus and polls the camera focus
     * (see {@link CameraController#syncFocus()}).
     */
    public void autoFocus() {
        final Network network = mCameraState.getNetwork();
        if (network == null) {
            Log.d(TAG, "Aborting auto focus (no network)");
            return;
        }

        mCommandThread.submit(new Runnable() {
            @Override
            public void run() {
                Response response = Requests.autoFocus(network);
                if (response.ok()) {
                    try {
                        Thread.sleep(AUTO_FOCUS_COOL_DOWN);
                        syncFocus();
                    } catch (InterruptedException e) {
                        Log.d(TAG, "Auto focus cool-down interrupted", e);
                    }
                } else {
                    showToast(R.string.camera_ctrl_af_failed);
                }
            }
        });
    }


    /**
     * Change the camera to the given focus point. This might might trigger a sequence of requests.
     * The continuation parameter is of special importance. It indicates whether the requested focus
     * change is part of a sequence of changes in the same direction, e.g., due to the user dragging
     * a focus bar.
     *
     * @param targetFocus  the target focus
     * @param useFastSteps whether to use fast focus steps (if supported)
     * @param continuation whether this is an update of a previous request to change the focus
     */
    public void setFocus(float targetFocus, final boolean useFastSteps, boolean continuation) {
        final Network network = mCameraState.getNetwork();
        if (network == null) {
            Log.d(TAG, "Aborting focus change (no network)");
            return;
        }

        mFocusLock.lock();

        // User input...
        if (continuation) {
            // This is an update of a previous user input. Thus, we can determine the direction of
            // the user input.
            mInputDirection = targetFocus < mTargetFocus
                    ? FocusDirection.TELE : FocusDirection.WIDE;
        } else {
            // This invocation is unrelated to the previous one. Reset any user input related data.
            mInputDirection = FocusDirection.NONE;
            mPassedTarget = false;
        }

        // Target focus...
        mTargetFocus = targetFocus;
        mTargetDirection = targetFocus < mCameraFocus
                ? FocusDirection.TELE : FocusDirection.WIDE;

        // Prevent a 'correction' of camera focus against the input direction after passing the
        // target focus. For instance, on input (..., 0.58, 0.59, 0.6) the controller might change
        // the camera focus in steps (..., 0.54, 0.58, 0.62) and thus pass the target focus. If the
        // user now issues an update of the target focus to 0.61, this update does not change the
        // input direction but it does change the target direction. Following up on this update will
        // most likely not move the camera focus closer to the target focus be perceived as glitch
        // by the user.
        boolean skipCorrection = mPassedTarget && mTargetDirection != mInputDirection;
        if (skipCorrection) {
            mFocusLock.unlock();
            Log.d(TAG, "Aborting focus change (skipping correction of previous focus change)");
            return;
        }

        // Do not schedule more than two tasks to change the focus. Tasks resubmit themselves until
        // they reach the current target. The second covers the scenario where the
        boolean workerActive = !mFocusSemaphore.tryAcquire();
        mFocusLock.unlock();
        if (workerActive) {
            Log.d(TAG, "Aborting focus change (cmd scheduled)");
            return;
        }

        mCommandThread.submit(new Runnable() {

            @Override
            public void run() {
                mFocusLock.lock();

                Lens lens = mCameraState.getLens();
                int target = (int) (lens.getFocusRange() * mTargetFocus);
                int position = lens.getFocusPoint();
                int distance = Math.abs(target - position);
                float stepSize = (float) Math.ceil(lens.getCurrentStepSize());

                Log.d(TAG, String.format(Locale.US,
                        "[%s] Requested focus change from %d to %d (distance %d, stepsize %.1f)",
                        System.identityHashCode(this), position, target, distance, stepSize));

                // Only issue a command if the expected new position is closer to the target than
                // the current.
                if (distance <= stepSize / 2.0) {
                    mFocusSemaphore.release();
                    mFocusLock.unlock();
                    Log.d(TAG, "Reached target (" + this + ")");
                    return;
                }

                // See explanation in enclosing method...
                boolean skipCorrection = mTargetDirection != mInputDirection && mPassedTarget;

                if (skipCorrection) {
                    mFocusSemaphore.release();
                    mFocusLock.unlock();
                    Log.d(TAG, "Passed target with previous command (" + this + ")");
                    return;
                }

                // Next is a blocking IO operation!
                mFocusLock.unlock();

                boolean stepFast = false;
                if (useFastSteps) {
                    float fastStepSize = (float) Math.ceil(lens.getCurrentFastStepSize());
                    stepFast = distance > fastStepSize;
                }

                FocusDirection direction;
                if (target < position) {
                    direction = stepFast ? FocusDirection.TELE_FAST : FocusDirection.TELE;
                } else {
                    direction = stepFast ? FocusDirection.WIDE_FAST : FocusDirection.WIDE;
                }

                int newPosition = changeFocus(direction);

                // Acquire lock again (target might have change in the meantime).
                mFocusLock.lock();
                target = (int) (lens.getFocusRange() * mTargetFocus);

                // To prevent creating an infinite loop that jumps from one side of the target to
                // the other and back, check whether we passed the target.
                boolean passedFromTele = position < target && newPosition >= target;
                boolean passedFromWide = position > target && newPosition <= target;
                mPassedTarget = passedFromTele || passedFromWide;
                mCameraFocus = (float) newPosition / lens.getFocusRange();

                if (mPassedTarget) {
                    mFocusSemaphore.release();
                    mFocusLock.unlock();
                    Log.d(TAG, "Passed target (" + this + ")");
                    return;
                }

                // Resubmit the task instead of directly issuing another request to prevent
                // blocking the command thread.
                mCommandThread.submit(this);
                mFocusLock.unlock();
            }
        });
    }

    /**
     * Goes through all commands on-by-one recording the responses. Creates a report consisting of
     * these recordings and some information about the Android device.
     *
     * @param callback the report callback
     */
    public void createReport(final ReportCallback callback) {
        final Network network = mCameraState.getNetwork();
        if (network == null) {
            Log.d(TAG, "Aborting focus change (no network)");
            return;
        }


        mCommandThread.submit(new Runnable() {
            @Override
            public void run() {
                Map<String, Response> responses = new LinkedHashMap<>(11);

                responses.put(Requests.CTRL_REQUEST_ACCESS,
                        Requests.requestAccess(network, UUID.randomUUID()));
                responses.put(Requests.SET_DEVICE_NAME, Requests.setDeviceName(network));
                responses.put(Requests.CMD_RECORD_MODE, Requests.changeToRecordMode(network));

                try {
                    Thread.sleep(REPORT_COOLDOWN);
                } catch (InterruptedException e) {
                    Log.d(TAG, "Report thread interrupted");
                }

                responses.put(Requests.GET_INFO_CAPABILITY, Requests.getCapabilities(network));
                responses.put(Requests.GET_INFO_LENS, Requests.getLensInfo(network));
                responses.put(Requests.GET_STATE, Requests.getState(network));
                responses.put(Requests.CMD_ONESHOT_AF, Requests.autoFocus(network));

                try {
                    Thread.sleep(REPORT_COOLDOWN);
                } catch (InterruptedException e) {
                    Log.d(TAG, "Report thread interrupted");
                }

                responses.put(Requests.CMD_FOCUS_TELE_NORMAL, Requests.focusTele(network));
                responses.put(Requests.CMD_FOCUS_TELE_FAST, Requests.focusTeleFast(network));
                responses.put(Requests.CMD_FOCUS_WIDE_NORMAL, Requests.focusWide(network));
                responses.put(Requests.CMD_FOCUS_WIDE_FAST, Requests.focusWideFast(network));

                StringBuilder builder = new StringBuilder();
                builder.append("Phone model: ");
                builder.append(Build.MODEL);
                builder.append("\n\nAndroid version: ");
                builder.append(Build.VERSION.RELEASE);
                builder.append("\n\nCamera responses (Base64 encoded):");

                for (Map.Entry<String, Response> entry: responses.entrySet()) {
                    String content = entry.getValue().content();
                    String data = "Request:\n" + entry.getKey() + "\nResponse:\n"
                            + (content == null ? "<empty>" : content);
                    data = Base64.encodeToString(data.getBytes(), Base64.DEFAULT).trim();
                    builder.append("\n\n");
                    builder.append(data);
                }

                callback.onFinish(builder.toString());
            }
        });
    }

    private void showToast(final int id) {
        // Android does not support displaying toasts from arbitrary background threads, thus
        // delegate calls to main looper.
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText(getApplicationContext(), id, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        });
    }

    private void startPollingThread() {
        final Network network = mCameraState.getNetwork();
        if (network == null) {
            Log.d(TAG, "Aborting start of polling thread (no network)");
            return;
        }

        // Prevent running multiple polling threads in parallel. Restarting this service might be
        // faster than shutting down a previous polling thread. Thus, wait for some time if no
        // permit could be acquired immediately.
        boolean acquired;
        try {
            acquired = POLLING_SEMAPHORE.tryAcquire(1, POLLING_PERMIT_TIMEOUT,
                    TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            acquired = false;
        }
        if (!acquired) {
            Log.d(TAG, "Aborting start of polling thread (failed to acquire permit)");
            return;
        }

        mPollingThread = new Thread(new Runnable() {

            private int mFailedRequests = 0;

            @Override
            public void run() {
                while (POLLING_SENTINEL.get()) {
                    Response response = Requests.getState(network);
                    if (readState(response)) {
                        // reset failure counter
                        mFailedRequests = 0;
                    } else {
                        mFailedRequests++;
                    }

                    if (mFailedRequests >= MAX_FAILED_REQUESTS) {
                        Log.d(TAG, "Polling thread stopped after " + MAX_FAILED_REQUESTS
                                + " failed requests");
                        mCameraState.unsetFlag(Flag.CONNECTED);
                        showToast(R.string.camera_ctrl_connection_loss);
                        break;
                    }

                    try {
                        Thread.sleep(POLLING_FREQUENCY);
                    } catch (InterruptedException e) {
                        Log.d(TAG, "Polling thread interrupted");
                    }
                }
                Log.d(TAG, "Stopping polling thread");
                POLLING_SEMAPHORE.release();
            }
        });

        Log.d(TAG, "Starting polling thread");
        POLLING_SENTINEL.set(true);
        mPollingThread.start();
    }

    private void stopPollingThread() {
        POLLING_SENTINEL.set(false);
        if (mPollingThread != null) {
            mPollingThread.interrupt();
            mPollingThread = null;
        }
    }

    private boolean isUnsupported(Response response) {
        if (response.ok()) {
            return false;
        } else {
            return response.httpCode() == HttpURLConnection.HTTP_NOT_FOUND
                    || Response.CODE_ERROR_NON_SUPPORT.equals(response.code())
                    || Response.CODE_ERROR_PARAM.equals(response.code());
        }
    }

    private boolean readAccessRequest(Response response) {
        boolean parsed = false;
        if (response.ok() && response.type() == Type.CSV) {
            CsvResponse csv = (CsvResponse) response;
            if (csv.hasStringAt(1)) {
                mCameraState.setModel(csv.stringAt(1));
                parsed = true;
            }
        }
        return parsed;
    }

    private boolean readState(Response response) {
        boolean parsed = false;
        if (response.ok() && response.type() == Type.XML) {
            XmlResponse xml = (XmlResponse) response;
            Map<String, Object> elements = xml.getElements();
            // Only accept recording mode as valid state.
            parsed = XmlResponse.VALUE_REC_MODE.equals(elements.get(XmlResponse.KEY_CAMERA_MODE));
        }
        return parsed;
    }

    private boolean readLensInfo(Response response) {
        boolean parsed = false;
        if (response.ok() && response.type() == Type.CSV) {
            CsvResponse csv = (CsvResponse) response;
            if (csv.hasIntAt(11) && csv.hasIntAt(12) && csv.hasIntAt(14) && csv.hasIntAt(15)) {
                int range = csv.intAt(15);
                int teleRange = csv.intAt(14);
                // The G85 answers with lens data regardless of whether a lens is connected. In that
                // case the range values are all zero.
                if (range > 0 && teleRange > 0 && range > teleRange) {
                    mCameraState.setLensInfo(csv.intAt(12), csv.intAt(11),
                            range, teleRange);
                    parsed = true;
                }
            }
        }
        return parsed;
    }

    private int readFocus(Response response) {
        int focus = -1;
        if (response.ok() && response.type() == Type.CSV) {
            CsvResponse csv = (CsvResponse) response;
            if (csv.hasIntAt(1)) {
                focus = csv.intAt(1);
                mCameraState.setFocusPoint(focus);
            }
        }
        return focus;
    }

    private int changeFocus(FocusDirection direction) {
        final Network network = mCameraState.getNetwork();
        if (network == null) {
            Log.d(TAG, "Aborting focus change (no network)");
            return -1;
        }

        Response response = null;
        switch (direction) {
            case TELE:
                response = Requests.focusTele(network);
                break;
            case TELE_FAST:
                response = Requests.focusTeleFast(network);
                break;
            case WIDE:
                response = Requests.focusWide(network);
                break;
            case WIDE_FAST:
                response = Requests.focusWideFast(network);
                break;
        }
        return  readFocus(response);
    }

    private int changeFocusOrThrow(FocusDirection direction) throws IOException {
        int result = changeFocus(direction);
        if (result == -1) {
            throw new IOException("Failed to change focus");
        }
        return result;
    }

}
