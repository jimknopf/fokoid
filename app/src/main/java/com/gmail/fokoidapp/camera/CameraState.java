/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp.camera;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;


/**
 * Singleton storing the camera state, e.g., the connection status and information about the lens.
 */
public class CameraState {

    private static final String TAG = "CameraState";

    public enum Flag {
        ONLINE,
        CONNECTING,
        CONNECTED,
        CALIBRATING,
        CALIBRATED
    }

    public enum Command {
        TELE,
        TELE_FAST,
        WIDE,
        WIDE_FAST,
        AUTO_FOCUS
    }

    /**
     * Callback for status changes.
     */
    public interface StatusCallback {

        /**
         * Called whenever a flag changes.
         *
         * @param flags the current flags
         */
        void onFlagUpdate(Set<Flag> flags);

        /**
         * Called whenever the lens information is updated.
         *
         * @param lens up-to-date lens information
         */
        void onLensUpdate(Lens lens);
    }

    /**
     * Callback for wifi networks. See {link @{@link CameraController#onCreate()}}.
     */
    private final ConnectivityManager.NetworkCallback mCallback = new ConnectivityManager.NetworkCallback(){
        @Override
        public void onAvailable(Network network) {
            mWifi = network;
            setFlag(Flag.ONLINE);
        }

        @Override
        public void onLost(Network network) {
            unsetFlags(EnumSet.of(Flag.ONLINE, Flag.CONNECTING, Flag.CONNECTED,
                    Flag.CALIBRATING));
            mWifi = null;
        }
    };

    private static CameraState sSingleton;

    /**
     * Returns the singleton instance managing the camera state.
     *
     * @param applicationContext the application context
     * @return the camera state
     */
    public synchronized static CameraState get(Context applicationContext) {
        if (sSingleton == null) {
            sSingleton = new CameraState(applicationContext);
        }
        return sSingleton;
    }

    private final EnumSet<Flag> mFlags = EnumSet.noneOf(Flag.class);
    private final EnumSet<Command> mCommands = EnumSet.noneOf(Command.class);
    private final Lens mLens = new Lens();
    private String mModel = null;
    private Network mWifi;

    private final List<StatusCallback> mCallbacks = new ArrayList<>();

    CameraState(Context context) {
        ConnectivityManager manager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkRequest requestWifi = new NetworkRequest.Builder()
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .build();
        manager.registerNetworkCallback(requestWifi, mCallback);
    }

    /**
     * Registers a status callback. The {@code initialUpdate} parameter can be used to request an
     * update right after registering (this does not affect other registered callbacks).
     *
     * @param callback the status callback
     * @param initialUpdate requests an initial update
     */
    public void registerCallback(StatusCallback callback, boolean initialUpdate) {
        Log.d(TAG, "Registering callback");
        synchronized (mCallbacks) {
            if (!mCallbacks.contains(callback)) {
                mCallbacks.add(callback);
            }
        }
        if (initialUpdate) {
            Log.d(TAG, "Firing initial update");
            Set<Flag> flags = EnumSet.copyOf(mFlags);
            Lens lens = new Lens(mLens);
            callback.onFlagUpdate(flags);
            callback.onLensUpdate(lens);
        }
    }

    /**
     * Unregisters the given status callback.
     *
     * @param callback the sttaus callback
     */
    public void unregisterCallback(StatusCallback callback) {
        Log.d(TAG, "Unregistering callback");
        synchronized (mCallbacks) {
            mCallbacks.remove(callback);
        }
    }

    /**
     * Return the current wifi connection. Unless the flag {@link Flag#CONNECTED} is set, there is
     * no guarantee the return network is actually the camera's wifi.
     *
     * @return the current wifi connection
     */
    public Network getNetwork() {
        return mWifi;
    }

    /**
     * @return the current set of flags
     */
    public Set<Flag> getFlags() {
        return Collections.unmodifiableSet(mFlags);
    }

    /**
     * @return the set of available commands
     */
    public Set<Command> getCommands() {
        return EnumSet.copyOf(mCommands);
    }

    /**
     * @return the current lens information
     */
    public Lens getLens() {
        return new Lens(mLens);
    }

    /**
     * @return the camera model descriptor or {@code null}
     */
    public String getModel() {
        return mModel;
    }

    void setFlag(Flag flag) {
        if (!mFlags.contains(flag)) {
            mFlags.add(flag);
            fireFlagUpdate();
        }
    }

    void unsetFlag(Flag flag) {
        if (mFlags.contains(flag)) {
            mFlags.remove(flag);
            fireFlagUpdate();
        }
    }

    void unsetFlags(Set<Flag> flags) {
        boolean changed = false;
        for (Flag flag: flags) {
            if (mFlags.contains(flag)) {
                mFlags.remove(flag);
                changed = true;
            }
        }
        if (changed) {
            fireFlagUpdate();
        }
    }

    void setCommand(Command command) {
        mCommands.add(command);
    }

    void setCommands(Set<Command> commands) {
        mCommands.addAll(commands);
    }

    void setModel(String model) {
        mModel = model;
    }

    void setLensInfo(int minFocalLength, int maxFocalLength, int focusRange, int teleRange) {
        mLens.setMinFocalLength(minFocalLength);
        mLens.setMaxFocalLength(maxFocalLength);
        mLens.setFocusRange(focusRange);
        mLens.setTeleMark(teleRange);
        mLens.setIdentified(true);

        fireLensUpdate();
    }

    void unsetLensInfo() {
        mLens.setIdentified(false);
        fireLensUpdate();
    }

    void setLensCalibration(float teleStepSize, float teleFastStepSize, float wideStepSize,
                            float wideFastStepSize) {
        mLens.setTeleStepSize(teleStepSize);
        mLens.setTeleFastStepSize(teleFastStepSize);
        mLens.setWideStepSize(wideStepSize);
        mLens.setWideFastStepSize(wideFastStepSize);
        mLens.setCalibrated(true);

        mFlags.add(Flag.CALIBRATED);

        fireFlagUpdate();
        fireLensUpdate();
    }

    void setFocusPoint(int focusPoint) {
        if (mLens.getFocusPoint() != focusPoint) {
            mLens.setFocusPoint(focusPoint);
            fireLensUpdate();
        }
    }

    private void fireFlagUpdate() {
        List<StatusCallback> copy;
        synchronized (mCallbacks) {
            copy = new ArrayList<>(mCallbacks);
        }

        final Set<Flag> flags = Collections.unmodifiableSet(EnumSet.copyOf(mFlags));
        Log.d(TAG, "Firing flag updates: " + flags);
        for (StatusCallback callback: copy) {
            callback.onFlagUpdate(flags);
        }
    }

    private void fireLensUpdate() {
        List<StatusCallback> copy;
        synchronized (mCallbacks) {
            copy = new ArrayList<>(mCallbacks);
        }

        final Lens lens = new Lens(mLens);
        Log.d(TAG, "Firing lens update: " + lens);
        for (StatusCallback callback: copy) {
            callback.onLensUpdate(lens);
        }
    }

}
