/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp.camera;

import android.net.Network;
import android.os.Build;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * Utility class for HTTP requests. All queries are executed in the foreground and block the thread
 * invoking the corresponding method.
 */
class Requests {

    static final String CTRL_REQUEST_ACCESS = "cam.cgi?mode=accctrl" +
            "&type=req_mode=accctrl&type=req_acc&value=%s&value2=%s";

    static final String GET_STATE = "cam.cgi?mode=getstate";

    static final String GET_INFO_CAPABILITY = "cam.cgi?mode=getinfo&type=capability";

    static final String GET_INFO_LENS = "cam.cgi?mode=getinfo&type=lens";

    static final String SET_DEVICE_NAME = "cam.cgi?mode=setsetting" +
            "&type=device_name&value=%s";

    static final String CMD_FOCUS_TELE_NORMAL = "cam.cgi?mode=camctrl" +
            "&type=focus&value=tele-normal";

    static final String CMD_FOCUS_TELE_FAST = "cam.cgi?mode=camctrl" +
            "&type=focus&value=tele-fast";

    static final String CMD_FOCUS_WIDE_NORMAL = "cam.cgi?mode=camctrl" +
            "&type=focus&value=wide-normal";

    static final String CMD_FOCUS_WIDE_FAST = "cam.cgi?mode=camctrl" +
            "&type=focus&value=wide-fast";

    static final String CMD_RECORD_MODE = "cam.cgi?mode=camcmd&value=recmode";

    static final String CMD_ONESHOT_AF = "cam.cgi?mode=camcmd&value=oneshot_af";

    private static final String TAG = "Requests";

    private static final int TIMEOUT_CTRL = 2500;

    private static final int TIMEOUT_GET_SET = 500;

    private static final int TIMEOUT_COMMAND = 500;

    private static final String HOST_FORMAT = "http://%s:80/";

    private static String sHost;

    static {
        setCameraIp("192.168.54.1");
    }
    
    static void setCameraIp(String ip) {
        sHost = String.format(HOST_FORMAT, ip.trim());
    }
    
    static Response requestAccess(Network network, UUID uuid) {
        URL url;
        try {
            String formattedUrl = String.format(sHost + CTRL_REQUEST_ACCESS,
                    uuid.toString(),
                    URLEncoder.encode(Build.MODEL, StandardCharsets.UTF_8.name()));
            url = new URL(formattedUrl);
        } catch (UnsupportedEncodingException | MalformedURLException e) {
            Log.e(TAG, "Tried to query malformed URL", e);
            throw new RuntimeException(e);
        }
        return getAndParse(network, url, TIMEOUT_CTRL);
    }

    static Response setDeviceName(Network network) {
        URL url;
        try {
            String formattedUrl = String.format(sHost + SET_DEVICE_NAME,
                    URLEncoder.encode(Build.MODEL, StandardCharsets.UTF_8.name()));
            url = new URL(formattedUrl);
        } catch (UnsupportedEncodingException | MalformedURLException e) {
            Log.e(TAG, "Tried to query malformed URL", e);
            throw new RuntimeException(e);
        }
        return getAndParse(network, url, TIMEOUT_GET_SET);
    }

    static Response getState(Network network) {
        return simpleCommand(network, GET_STATE);
    }

    static Response getLensInfo(Network network) {
        return simpleCommand(network, GET_INFO_LENS);
    }

    static Response getCapabilities(Network network) {
        return simpleCommand(network, GET_INFO_CAPABILITY);
    }

    static Response changeToRecordMode(Network network) {
        return simpleCommand(network, CMD_RECORD_MODE);
    }

    static Response focusTele(Network network) {
        return simpleCommand(network, CMD_FOCUS_TELE_NORMAL);
    }

    static Response focusWide(Network network) {
        return simpleCommand(network, CMD_FOCUS_WIDE_NORMAL);
    }

    static Response focusTeleFast(Network network) {
        return simpleCommand(network, CMD_FOCUS_TELE_FAST);
    }

    static Response focusWideFast(Network network) {
        return simpleCommand(network, CMD_FOCUS_WIDE_FAST);
    }

    static Response autoFocus(Network network) {
        return simpleCommand(network, CMD_ONESHOT_AF);
    }

    private static Response getAndParse(Network network, URL url, int timeout) {
        Log.d(TAG, "Querying " + url.toString());
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) network.openConnection(url);
            connection.setConnectTimeout(timeout);
            Response response = Response.of(connection);
            Log.d(TAG, "Received response " + response);
            return response;
        } catch (IOException e) {
            Log.d(TAG, "Query failed", e);
            return Response.dummyResponse();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    private static Response simpleCommand(Network network, String command) {
        URL url;
        try {
            url = new URL(sHost + command);
        } catch (MalformedURLException e) {
            Log.e(TAG, "Tried to query malformed URL", e);
            throw new RuntimeException(e);
        }
       return getAndParse(network, url, TIMEOUT_COMMAND);
    }

}
