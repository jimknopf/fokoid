/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp.camera;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;

/**
 * Represents a camera response. To access the response body, see its subclasses {@link CsvResponse}
 * and {@link XmlResponse}.
 */
abstract class Response {

    static final String CODE_OK = "ok";
    static final String CODE_ERROR_NON_SUPPORT = "err_non_support";
    static final String CODE_ERROR_PARAM = "err_param";

    private static final int MAX_CONTENT_PRINT_LENGTH = 64;

    enum Type {
        /** Response of content-type {@code text/plain}, interpreted as single-line CSV data. */
        CSV,
        /** Response of content-type {@code text/xml}. */
        XML,
        /** Response of unknown content-type or for which parsing failed. */
        EMPTY
    }

    private final int mHttpCode;

    /**
     * Utility constructor to remember HTTP code.
     *
     * @param httpCode the http code
     */
    Response(int httpCode) {
        mHttpCode = httpCode;
    }

    /**
     * HTTP code returned by the camera.
     *
     * @return the return code or {@code -1}
     */
    int httpCode() {
        return mHttpCode;
    }

    /**
     * String return code used by the camera (if any), e.g., {@code ok} or {@code err_reject}.
     *
     * @return the return code or {@code null}
     */
    abstract String code();

    /**
     * The response raw response body (if any). See subclasses for parsed content.
     *
     * @return the response body or {@code null}
     */
    abstract String content();

    /**
     * Whether the camera responded with a success code. Subclasses might perform additional
     * content-aware checks.
     *
     * @return {@code true} iff the camera responded with a success code
     */
    abstract boolean ok();

    /**
     * The type (subclass) of the response.
     *
     * @return the response type
     */
    abstract Type type();

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("{ok=");
        builder.append(ok());
        builder.append(", code='");
        builder.append(code());
        builder.append("', content='");
        String content = content();
        if (content.length() > MAX_CONTENT_PRINT_LENGTH) {
            builder.append(content.substring(0, MAX_CONTENT_PRINT_LENGTH));
            builder.append("...");
        } else {
            builder.append(content);
        }
        builder.append("'}");
        return builder.toString();
    }

    /**
     * Creates an empty dummy response that always answers {@link Response#ok()} with {@code false}.
     *
     * @return an empty dummy response
     */
    static Response dummyResponse() {
        return new Response(-1) {
            @Override
            Type type() {
                return Type.EMPTY;
            }

            @Override
            boolean ok() {
                return false;
            }

            @Override
            String code() {
                return null;
            }

            @Override
            String content() {
                return null;
            }
        };
    }

    /**
     * Creates a specialized response based on the given connection's content-type.
     *
     * @param connection the http connection
     * @return the specialized response
     * @throws IOException if the content-type is unknown
     */
    static Response of(HttpURLConnection connection) throws IOException {
        int code = connection.getResponseCode();
        String body = readStream(connection.getInputStream());
        String type = connection.getContentType();
        if ("text/plain".equals(type)) {
            return new CsvResponse(code, body);
        } else if ("text/xml".equals(type)) {
            return new XmlResponse(code, body);
        } else {
            throw new IOException("Unknown content type");
        }
    }

    private static String readStream(InputStream in) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        return builder.toString();
    }
}
