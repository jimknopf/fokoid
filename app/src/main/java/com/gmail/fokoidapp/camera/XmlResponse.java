/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp.camera;

import android.util.Log;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Specialized response for content type {@code text/xml}. Extracts information such as the return
 * code and supported commands.
 */
class XmlResponse extends Response {

    static final String KEY_CODE = "code";
    static final String KEY_BATTERY_LEVEL = "battery_level";
    static final String KEY_CAMERA_MODE = "camera_mode";
    static final String KEY_RECORDING = "recording";
    static final String KEY_COMMANDS = "commands";
    static final String KEY_SPECIFICATIONS = "specifications";

    static final String VALUE_ON = "on";
    static final String VALUE_OFF = "off";
    static final String VALUE_REC_MODE = "rec";

    static final String ITEM_CMD_TELE = "tele-normal";
    static final String ITEM_CMD_WIDE = "wide-normal";
    static final String ITEM_CMD_TELE_FAST = "tele-fast";
    static final String ITEM_CMD_WIDE_FAST = "wide-fast";
    static final String ITEM_SPEC_ONESHOT_AF = "oneshot_af_enable";

    private static final String TAG = "XmlResponse";

    private static final Map<String, String> PATHS_KEY_VALUE = new HashMap<>(4);
    static {
        PATHS_KEY_VALUE.put("/camrply/result", KEY_CODE);
        PATHS_KEY_VALUE.put("/camrply/state/batt", KEY_BATTERY_LEVEL);
        PATHS_KEY_VALUE.put("/camrply/state/cammode", KEY_CAMERA_MODE);
        PATHS_KEY_VALUE.put("/camrply/state/rec", KEY_RECORDING);
    }

    private static final Map<String, String> PATHS_KEY_VALUE_SET = new HashMap<>(2);
    static {
        PATHS_KEY_VALUE_SET.put("/camrply/camcmdlist/camcmd", KEY_COMMANDS);
        PATHS_KEY_VALUE_SET.put("/camrply/camspeclist/camspec", KEY_SPECIFICATIONS);
    }

    /**
     * Simple SAX handler that keeps track of the current path to identify elements of interest
     * defined in {@link XmlResponse#PATHS_KEY_VALUE} and {@link XmlResponse#PATHS_KEY_VALUE_SET}.
     * Character nodes whose path match are added to a key value (set) mapping.
     */
    private static class Handler extends DefaultHandler {

        private Map<String, Object> mElements;
        private String mPath;

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (PATHS_KEY_VALUE.containsKey(mPath)) {
                String key = PATHS_KEY_VALUE.get(mPath);
                String value = String.valueOf(ch, start, length);
                value = value.trim();
                mElements.put(key, value);
            } else if (PATHS_KEY_VALUE_SET.containsKey(mPath)) {
                String key = PATHS_KEY_VALUE_SET.get(mPath);
                String item = String.valueOf(ch, start, length);
                item = item.trim();

                Set<String> items = (Set<String>) mElements.get(key);
                if (items == null) {
                    items = new HashSet<>();
                    mElements.put(key, items);
                }

                items.add(item);
            }
        }

        @Override
        public void startDocument() throws SAXException {
            mElements = new HashMap<>();
            mPath = "";
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes)
                throws SAXException {
            mPath += "/" + localName;
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            mPath = mPath.substring(0, mPath.length() - localName.length() - 1);
        }

        Map<String, Object> getElements() {
            return mElements;
        }
    }

    private static final SAXParserFactory PARSER_FACTORY = SAXParserFactory.newInstance();

    private final String mContent;
    private final Map<String, Object> mElements;
    private final String mCode;
    private final boolean mOk;

    XmlResponse(int httpCode, String body) {
        super(httpCode);

        mContent = body.trim();

        Handler handler = new Handler();
        boolean parsed = false;
        try {
            SAXParser parser = PARSER_FACTORY.newSAXParser();

            XMLReader reader = parser.getXMLReader();
            reader.setContentHandler(handler);
            reader.parse(new InputSource(new StringReader(mContent)));

            // only consider parsing successful if a result code was extracted
            Map<String, Object> elements = handler.getElements();
            parsed = elements != null && elements.containsKey(KEY_CODE);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            Log.d(TAG, "Failed to parse XML response", e);
        }

        if (parsed) {
            mElements = handler.getElements();
            mCode = (String) mElements.get(KEY_CODE);
            mOk = httpCode >= 200 && httpCode < 300 && CODE_OK.equals(mCode);
        } else {
            mElements = null;
            mCode = null;
            mOk = false;
        }
    }

    @Override
    Type type() {
        return Type.XML;
    }

    @Override
    boolean ok() {
        return mOk;
    }

    @Override
    String code() {
        return mCode;
    }

    @Override
    String content() {
        return mContent;
    }

    /**
     * Returns the key value (set) mapping extracted from the XML response.
     *
     * @return the key value mapping
     */
    Map<String, Object> getElements() {
        return mElements;
    }
}
