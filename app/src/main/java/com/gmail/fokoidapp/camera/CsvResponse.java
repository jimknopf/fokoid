/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp.camera;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Specialized response for content type {@code text/plain} assuming single-line CSV data.
 */
class CsvResponse extends Response {

    static final String VALUE_UNDER_RESEARCH = "ok_under_research";

    private final static Pattern DELIMITER = Pattern.compile("\\s*[,|/]\\s*");

    private final String mContent;
    private final List<Object> mValues;
    private final String mCode;
    private final boolean mOK;

    CsvResponse(int httpCode, String body) {
        super(httpCode);

        mContent = body.trim();

        Scanner scanner = new Scanner(mContent);
        scanner.useDelimiter(DELIMITER);
        mValues = new ArrayList<>();
        while (scanner.hasNext()) {
            if (scanner.hasNextInt()) {
                mValues.add(scanner.nextInt());
            } else {
                mValues.add(scanner.next());
            }
        }
        scanner.close();

        if (mValues.isEmpty() || !(mValues.get(0) instanceof String)) {
            mCode = null;
            mOK = false;
        } else {
            mCode = (String) mValues.get(0);
            mOK = httpCode >= 200 && httpCode < 300 && CODE_OK.equals(mCode);
        }
    }

    @Override
    Type type() {
        return Type.CSV;
    }

    @Override
    boolean ok() {
        return mOK;
    }

    @Override
    String code() {
        return mCode;
    }

    @Override
    String content() {
        return mContent;
    }

    /**
     * Checks whether the response contains a string at the given position.
     *
     * @param field the field index
     * @return {@code true} iff the field exists and contains a string
     */
    public boolean hasStringAt(int field) {
        return field < mValues.size() && mValues.get(field) instanceof String;
    }

    /**
     * Attempts to read a string from the field with the given index.
     *
     * @param field the field index
     * @return the value of the field as string
     * @throws IndexOutOfBoundsException if there is no field with the given index
     * @throws ClassCastException if the field does not contain a string
     */
    public String stringAt(int field) {
        return (String) mValues.get(field);
    }

    /**
     * Checks whether the response contains an integer at the given position.
     *
     * @param field the field index
     * @return {@code true} iff the field exists and contains an integer
     */
    public boolean hasIntAt(int field) {
        return field < mValues.size() && mValues.get(field) instanceof Integer;
    }

    /**
     * Attempts to read an integer from the field with the given index.
     *
     * @param field the field index
     * @return the value of the field as integer
     * @throws IndexOutOfBoundsException if there is no field with the given index
     * @throws ClassCastException if the field does not contain an integer
     */
    public int intAt(int field) {
        return (Integer) mValues.get(field);
    }

}
