/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp.marks;

import java.util.Arrays;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Maps user defined marks to the range [0, 1] such that the leftmost and rightmost marks are close
 * to 0 and 1 respectively. Supports sorting of marks and smooth interpolation of positions in
 * between marks.
 */
public class Mapping {

    /** (Relative) padding of the left- and rightmost mark the from interval borders. */
    private static final float MARK_PADDING = 0.1f;

    private final float[] mMarks;
    private final float[] mMarkPositions;
    private final int[] mOrderedIndices;
    private final int mNMarks;
    private final float mLength;
    private final boolean mSorted;
    private final boolean mSmooth;

    public Mapping(float[] marks, boolean sorted, boolean smooth) {
        int nUserMarks = marks.length;
        mNMarks = nUserMarks + 2;
        mSorted = sorted;
        mSmooth = smooth;

        // Sort given marks if requested:
        float[] userMarks;
        int[] indices;
        if (sorted) {
            userMarks = new float[nUserMarks];
            indices = new int[nUserMarks];
            SortedMap<Float, Integer> map = new TreeMap<>();
            for (int i = 0; i < nUserMarks; i++) {
                map.put(marks[i], i);
            }
            int i = 0;
            for (Map.Entry<Float, Integer> entry: map.entrySet()) {
                userMarks[i] = entry.getKey();
                indices[i] = entry.getValue();
                i++;
            }
        } else {
            userMarks = marks;
            indices = new int[nUserMarks];
            for (int i = 0; i < nUserMarks; i++) {
                indices[i] = i;
            }
        }

        // Compute distance covered by traversing all marks: if the marks are unsorted, this is not
        // the same as the absolute distance between the leftmost and rightmost mark.
        float distance = 0f;
        float previousMark = userMarks[0];
        for (int i = 1; i < nUserMarks; i++) {
            float currentMark = userMarks[i];
            distance += Math.abs(currentMark - previousMark);
            previousMark = currentMark;
        }

        // Add padding to the left- and rightmost marks, but do not leave [0, 1] range.
        float padding = distance * MARK_PADDING;

        float start = userMarks[0];
        start = start < userMarks[1] ? start - padding : start + padding;
        start = Math.max(0f, Math.min(start, 1f));

        float end = userMarks[nUserMarks - 1];
        end = end > userMarks[nUserMarks - 2] ? end + padding : end - padding;
        end = Math.max(0f, Math.min(end, 1f));

        // The padding adds to 'invisible' marks that change the distance calculated before.
        distance += Math.abs(start - userMarks[0]) + Math.abs(userMarks[nUserMarks - 1] - end);

        mLength = distance;

        float[] allMarks = new float[mNMarks];
        allMarks[0] = start;
        System.arraycopy(userMarks, 0, allMarks, 1, nUserMarks);
        allMarks[mNMarks - 1] = end;

        // Normalize marks such that the (invisible) left and right bounds added by the padding
        // are mapped to 0 and 1 respectively.
        float[] allMarkPositions = new float[mNMarks];
        float coveredDistance = 0f;
        previousMark = 0f;
        for (int i = 0; i < mNMarks; i++) {
            float currentMark = allMarks[i];
            coveredDistance += Math.abs(currentMark - previousMark);
            allMarkPositions[i] = Math.max(0f, Math.min((coveredDistance - start) / distance, 1f));
            previousMark = currentMark;
        }

        mMarks = allMarks;
        mMarkPositions = allMarkPositions;
        mOrderedIndices = indices;
    }

    /**
     * Returns the length of the mapping, i.e., the distance covered by moving the focus from
     * {@code mapPosition(0f, ...)} to {@code mapPosition(1f, ...)}.
     *
     * @return the length of the mapping
     */
    public float getLength() {
        return mLength;
    }

    /**
     * Returns the mark positions wrapped by this mapping.
     *
     * @return the mark positions
     */
    public float[] getPositions() {
        return Arrays.copyOfRange(mMarkPositions, 1, mNMarks - 1);
    }

    /**
     * Returns the indices of the user defined marks in the internal order used by this mapping.
     *
     * @return the ordered mark indices
     */
    public int[] getOrder() {
        return mOrderedIndices;
    }

    /**
     * Whether the mapping is sorted, i.e., marks are returned in order.
     *
     * @return {@code true} iff the mapping is sorted
     */
    public boolean isSorted() {
        return mSorted;
    }

    /**
     * Maps the given position in the [0, 1] range to a focus value. If the given position lies in
     * between two marks, the focus value is interpolated using either linear interpolation or
     * smooth stepping.
     *
     * @param position the position to map
     * @return the interpolated focus value
     */
    public float mapPosition(float position) {
        // The mapping assumes the input position in the range [0, 1]
        position = Math.max(0f, Math.min(position, 1f));

        // Enforce hard stops for the left- and rightmost marks (not counting the bounds).
        if (position <= mMarkPositions[1]) {
            return mMarks[1];
        }
        if (position >= mMarkPositions[mNMarks - 2]) {
            return mMarks[mNMarks - 2];
        }

        // Find enclosing marks...
        int rightBound = 2;
        while (mMarkPositions[rightBound] < position) {
            rightBound++;
        }
        int leftBound = rightBound - 1;

        // Interpolate between bounding marks, smooth relative position if requested.
        float distLeftToRight = Math.abs(mMarkPositions[rightBound] - mMarkPositions[leftBound]);
        float distLeftToPos = Math.abs(position - mMarkPositions[leftBound]);
        float factor = distLeftToPos / distLeftToRight;
        if (mSmooth) {
            factor = smooth(factor);
        }

        return (1f - factor) * mMarks[leftBound] + factor * mMarks[rightBound];
    }

    /**
     * Calculates all position for which the focus matches the given value. If the mapping is
     * sorted, the result will contain at most one position.
     *
     * @param focus the focus point to look up
     * @return the list of matching positions
     */
    public float[] mapFocus(float focus) {
        float[] positions = new float[mNMarks - 1];
        int matches = 0;
        for (int i = 0; i < mNMarks - 1; i++) {
            float leftFocus = mMarks[i];
            float rightFocus = mMarks[i + 1];
            if (leftFocus < rightFocus) {
                if (leftFocus < focus && focus <= rightFocus) {
                    float distLeftToRight = rightFocus - leftFocus;
                    float distLeftToFocus = focus - leftFocus;
                    float factor = distLeftToFocus / distLeftToRight;
                    positions[matches++] = (1f - factor) * mMarkPositions[i]
                            + factor * mMarkPositions[i + 1];
                }
            } else {
                if (leftFocus > focus && focus >= rightFocus) {
                    float distLeftToRight = leftFocus - rightFocus;
                    float distLeftToFocus = leftFocus - focus;
                    float factor = distLeftToFocus / distLeftToRight;
                    positions[matches++] = (1f - factor) * mMarkPositions[i]
                            + factor * mMarkPositions[i + 1];
                }
            }
        }
        return Arrays.copyOf(positions, matches);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("{sorted = ");
        builder.append(mSorted);
        builder.append(", length = ");
        builder.append(mLength);
        builder.append(" , marks = {");
        for (int i = 0; i < mNMarks; i++) {
            builder.append(mMarkPositions[i]);
            builder.append(": ");
            builder.append(mMarks[i]);
            if (i < mNMarks - 1) {
                builder.append(", ");
            }
        }
        builder.append("}}");
        return builder.toString();
    }

    /** Classic smooth-step function {@code s(x) = 3*x^2 - 2*x^3. */
    private float smooth(float x) {
        return (3f - 2f * x) * x * x;
    }

}
