/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp.marks;

import android.util.Log;

import java.util.Arrays;

/**
 * Singleton managing up to {@value MAX_MARKS} user defined marks (normalized to [0, 1]).
 */
public class Marks {

    /** This container will always contain at least {@value} marks. */
    public final static int MIN_MARKS = 2;

    /** This container will contain at most {@value} marks. */
    public final static int MAX_MARKS = 5;

    private final static String TAG = "Marks";

    /** Marks are considered duplicates if the distance between them is smaller than {@value}. */
    private final static float EPSILON = 0.01f;

    private static Marks sSingleton;

    public synchronized static Marks get() {
        if (sSingleton == null) {
            sSingleton = new Marks();
        }
        return sSingleton;
    }

    private final float[] mMarks;
    private int mSize;

    private Marks() {
        mMarks = new float[MAX_MARKS];
        mMarks[0] = 0.25f;
        mMarks[1] = 0.75f;
        mSize = MIN_MARKS;
    }

    /**
     * Adds a new mark if there are les than {@value MAX_MARKS} marks and the new mark does not
     * duplicate an existing one.
     *
     * @param mark the mark to be added
     * @return {@code true} iff the given mark was added
     */
    public boolean add(float mark) {
        boolean added = false;
        if (mSize < MAX_MARKS) {
            float sanitized = Math.min(Math.max(0f, mark), 1f);
            boolean duplicate = isDuplicate(mSize, sanitized);
            if (duplicate) {
                Log.d(TAG, "Ignoring new mark (duplicates existing)");
            } else {
                mMarks[mSize] = sanitized;
                mSize++;
                added = true;
            }
        }
        return added;
    }

    /**
     * Returns the mark with the given index.
     *
     * @param index the index to look up
     * @return the mark
     */
    public float get(int index) {
        return mMarks[index];
    }

    /**
     * @return array containing all marks
     */
    public float[] getAll() {
        return Arrays.copyOf(mMarks, mSize);
    }

    /**
     * @return {@code true} iff at maximum capacity of {@value MAX_MARKS} marks
     */
    public boolean isAtMaxCapacity() {
        return mSize == MAX_MARKS;
    }

    /**
     * @return {@code true} iff at minimum capacity of {@value MIN_MARKS} marks
     */
    public boolean isAtMinCapacity() {
        return mSize == MIN_MARKS;
    }

    /**
     * Removes the mark at the given index iff there are more than {@value MIN_MARKS} marks.
     *
     * @param index the mark to remove
     * @return {@code true} iff the marks was removed
     */
    public boolean remove(int index) {
        if (index < 0 || index >= mSize) {
            throw new IndexOutOfBoundsException();
        }
        if (mSize > MIN_MARKS) {
            if (index < mSize - 1) {
                float[] tmp = Arrays.copyOf(mMarks, MAX_MARKS);
                System.arraycopy(tmp, index + 1, mMarks, index, MAX_MARKS - index - 1);
            }
            mSize--;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return the number of marks
     */
    public int size() {
        return mSize;
    }

    /**
     * Swaps the marks at the given indices.
     *
     * @param first first index
     * @param second secodn index
     */
    public void swap(int first, int second) {
        float tmp = mMarks[first];
        mMarks[first] = mMarks[second];
        mMarks[second] = tmp;
    }

    /**
     * Updates the mark at the given index if the new value does not duplicate another mark.
     *
     * @param index the mark to update
     * @param mark the new value
     * @return {@code true} iff the mark was updated
     */
    public boolean update(int index, float mark) {
        if (index < 0 || index >= mSize) {
            throw new IndexOutOfBoundsException();
        }
        float sanitized = Math.min(Math.max(0f, mark), 1f);
        boolean duplicate = isDuplicate(index, sanitized);
        if (duplicate) {
            Log.d(TAG, "Ignoring mark update (duplicates existing)");
            return false;
        } else {
            mMarks[index] = sanitized;
            return true;
        }
    }

    private boolean isDuplicate(int index, float mark) {
        boolean found = false;
        for (int i = 0; i < mSize; i++) {
            if (i != index && Math.abs(mMarks[i] - mark) < EPSILON) {
                found = true;
                break;
            }
        }
        return found;
    }

}
