/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.TextView;

import java.util.Set;

import com.gmail.fokoidapp.camera.CameraController;
import com.gmail.fokoidapp.camera.CameraController.CameraControllerBinder;
import com.gmail.fokoidapp.camera.CameraState;
import com.gmail.fokoidapp.camera.CameraState.Flag;
import com.gmail.fokoidapp.camera.CameraState.StatusCallback;
import com.gmail.fokoidapp.camera.Lens;
import com.gmail.fokoidapp.widget.FocusBar;

/**
 * The app's home screen. Shows the connection status and the last known focus position. Allows to
 * (re)connect to the camera and to perform a calibration of the lens.
 */
public class Home extends Activity {

    /**
     * Callback for camera status changes. En- or disables elements upon changes of the connection
     * and updates the focus visualization.
     */
    private final StatusCallback mStatusCallback = new StatusCallback() {

        @Override
        public void onFlagUpdate(final Set<CameraState.Flag> flags) {
            final boolean online = flags.contains(Flag.ONLINE);
            final boolean connected = flags.contains(Flag.CONNECTED);
            final boolean calibrated = flags.contains(Flag.CALIBRATED);
            final boolean busy = flags.contains(Flag.CONNECTING)
                    || flags.contains(Flag.CALIBRATING);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mNetworkIndicator.setChecked(online);
                    mConnectionIndicator.setChecked(connected);
                    mCalibratedIndicator.setChecked(calibrated);
                    mFocusIndicator.setIndeterminate(!connected);

                    if (connected) {
                        Lens lens = mCameraState.getLens();
                        String model = mCameraState.getModel();
                        if (model == null) {
                            model = getString(R.string.home_default_model);
                        }
                        String modelInfo;
                        if (lens.getMinFocalLength() == lens.getMaxFocalLength()) {
                            modelInfo = getString(R.string.home_model_info_prime, model,
                                    lens.getMinFocalLength());
                        } else {
                            modelInfo = getString(R.string.home_model_info, model,
                                    lens.getMinFocalLength(), lens.getMaxFocalLength());
                        }
                        mModelInfo.setText(modelInfo);
                        mModelInfo.setVisibility(View.VISIBLE);
                        mConnectButton.setText(mReconnectLabel);

                    } else {
                        mConnectButton.setText(mConnectLabel);
                        mModelInfo.setVisibility(View.INVISIBLE);
                    }

                    if (online) {
                        mConnectButton.setEnabled(!busy);
                        mCalibrateButton.setEnabled(connected && !busy);
                    } else {
                        mConnectButton.setEnabled(false);
                        mCalibrateButton.setEnabled(false);
                    }
                }
            });
        }

        @Override
        public void onLensUpdate(Lens lens) {
            if (lens.isIdentified()) {
                mFocusIndicator.setMax(lens.getFocusRange());
                mFocusIndicator.setProgress(lens.getFocusPoint());
                mFocusIndicator.postInvalidate();
            }
        }
    };

    /** Connection to {@link CameraController} service. */
    private final ServiceConnection mCameraControllerConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            CameraControllerBinder binder = (CameraControllerBinder) service;
            mCameraController = binder.get();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mCameraController = null;
        }
    };

    private CameraState mCameraState;
    private CameraController mCameraController;

    private String mConnectLabel;
    private String mReconnectLabel;

    private CheckedTextView mNetworkIndicator;
    private CheckedTextView mConnectionIndicator;
    private CheckedTextView mCalibratedIndicator;
    private FocusBar mFocusIndicator;
    private TextView mModelInfo;

    private Button mConnectButton;
    private Button mCalibrateButton;

    private Dialog mDisclaimerDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mConnectLabel = getString(R.string.home_action_connect);
        mReconnectLabel = getString(R.string.home_action_reconnect);

        mNetworkIndicator = (CheckedTextView) findViewById(R.id.wifiIndicator);
        mConnectionIndicator = (CheckedTextView) findViewById(R.id.cameraControlIndicator);
        mCalibratedIndicator = (CheckedTextView) findViewById(R.id.calibrationIndicator);
        mFocusIndicator = (FocusBar) findViewById(R.id.focusIndicator);
        mModelInfo = (TextView) findViewById(R.id.modelInfo);

        mConnectButton = (Button) findViewById(R.id.connectButton);
        mCalibrateButton = (Button) findViewById(R.id.calibrateButton);

        mCameraState = CameraState.get(getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent bindController = new Intent(this, CameraController.class);
        bindService(bindController, mCameraControllerConnection, BIND_AUTO_CREATE);

        mCameraState.registerCallback(mStatusCallback, true);

        if (!getPreferences(MODE_PRIVATE)
                .getBoolean(getString(R.string.home_key_warranty_disclaimer_shown), false)) {
            showWarrantyDisclaimer();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mCameraState.unregisterCallback(mStatusCallback);
        unbindService(mCameraControllerConnection);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean consumed = false;
        switch (item.getItemId()) {
            case R.id.action_marks:
                Intent marks = new Intent(this, MarkList.class);
                startActivity(marks);
                consumed = true;
                break;
            case R.id.action_follow_focus:
                Intent followFocus = new Intent(this, FollowFocus.class);
                startActivity(followFocus);
                consumed = true;
                break;
            case R.id.getting_started:
                Intent gettingStarted = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(getString(R.string.home_getting_started_url)));
                startActivity(gettingStarted);
                consumed = true;
                break;
            case R.id.action_report:
                Intent report = new Intent(this, Report.class);
                startActivity(report);
                consumed = true;
                break;
            case R.id.action_settings:
                Intent settings = new Intent(this, Settings.class);
                startActivity(settings);
                consumed = true;
                break;
        }
        return consumed || super.onOptionsItemSelected(item);
    }

    public void onConnect(View view) {
        if (mCameraController != null) {
            mCameraController.connect();
        }
    }

    public void onCalibrate(View view) {
        if (mCameraController != null) {
            mCameraController.calibrate();
        }
    }

    private void showWarrantyDisclaimer() {
        final SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        final String disclaimerKey = getString(R.string.home_key_warranty_disclaimer_shown);
        if (mDisclaimerDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            mDisclaimerDialog = builder.setTitle(R.string.app_name)
                    .setMessage(R.string.home_warranty_disclaimer)
                    .setPositiveButton(R.string.home_action_continue,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putBoolean(disclaimerKey, true);
                                    editor.apply();
                                    mDisclaimerDialog = null;
                                }
                            })
                    .setNegativeButton(R.string.home_action_close_app,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mDisclaimerDialog = null;
                                    finish();
                                }
                            })
                    .setCancelable(false)
                    .create();
        }
        if (!mDisclaimerDialog.isShowing()) {
            mDisclaimerDialog.show();
        }
    }
}
