/*
 * Fokoid - a follow focus app for Android.
 * Copyright (C) 2017 Michael Knopf
 *
 * This file is part of Fokoid.
 *
 * Fokoid is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fokoid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package com.gmail.fokoidapp;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import java.util.Set;

import com.gmail.fokoidapp.camera.CameraController;
import com.gmail.fokoidapp.camera.CameraController.CameraControllerBinder;
import com.gmail.fokoidapp.camera.CameraState;
import com.gmail.fokoidapp.camera.CameraState.Flag;
import com.gmail.fokoidapp.camera.CameraState.StatusCallback;
import com.gmail.fokoidapp.camera.Lens;
import com.gmail.fokoidapp.widget.FocusBar;

/**
 * Activity to create or edit a mark. Allows to sync mark with current camera focus (if available).
 */
public class MarkEditor extends Activity {

    /** Intent extra data key for a focus value. */
    public static final String KEY_MARK = "focus";

    /** Intent extra data key for an index. */
    public static final String KEY_INDEX = "index";

    /**
     * Callback to handle camera and lens updates. Disables camera control elements if no connection
     * is available of the camera is busy.
     */
    private final StatusCallback mStatusCallback = new StatusCallback() {
        @Override
        public void onFlagUpdate(Set<CameraState.Flag> flags) {
            boolean connected = flags.contains(Flag.CONNECTED);
            boolean busy = flags.contains(Flag.CONNECTING) || flags.contains(Flag.CALIBRATING);
            final boolean cameraReady = connected && !busy;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSyncButton.setEnabled(cameraReady);
                    mAutoFocusButton.setEnabled(cameraReady);
                }
            });
        }

        @Override
        public void onLensUpdate(Lens lens) {
            if (lens.isIdentified()) {
                mCameraValue = (float) lens.getFocusPoint() / lens.getFocusRange();
            } else {
                mCameraValue = 0f;
            }
            mCameraBar.setProgress((int) (mCameraBar.getMax() * mCameraValue));
            mCameraBar.postInvalidate();
        }
    };

    /** Connection to {@link CameraController} service. */
    private final ServiceConnection mCameraControllerConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            CameraControllerBinder binder = (CameraControllerBinder) service;
            mCameraController = binder.get();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mCameraController = null;
        }
    };

    private CameraState mCameraState;
    private CameraController mCameraController;

    private FocusBar mMarkBar;
    private FocusBar mCameraBar;
    private ImageButton mSyncButton;
    private ImageButton mAutoFocusButton;

    private float mMark = 0.5f;
    private float mCameraValue = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_editor);

        mMarkBar = (FocusBar) findViewById(R.id.markFocus);
        mCameraBar = (FocusBar) findViewById(R.id.cameraFocus);
        mSyncButton = (ImageButton) findViewById(R.id.sync);
        mAutoFocusButton = (ImageButton) findViewById(R.id.autoFocus);

        mCameraState = CameraState.get(getApplicationContext());

        Intent request = getIntent();
        mMark = request.getFloatExtra(KEY_MARK, 0.5f);

        updateMarkFocus();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_mark_editor, menu);
        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();
        Intent bindController = new Intent(this, CameraController.class);
        bindService(bindController, mCameraControllerConnection, BIND_AUTO_CREATE);
        mCameraState.registerCallback(mStatusCallback, true);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mCameraState.unregisterCallback(mStatusCallback);
        unbindService(mCameraControllerConnection);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean consumed = false;
        switch (item.getItemId()) {
            case R.id.action_create_mark:
                Intent request = getIntent();
                Intent result = new Intent(this, MarkList.class);
                if (request.hasExtra(KEY_INDEX)) {
                    int index = request.getIntExtra(KEY_INDEX, 0);
                    result.putExtra(KEY_INDEX, index);
                }
                result.putExtra(KEY_MARK, mMark);
                setResult(Activity.RESULT_OK, result);
                finish();
                consumed = true;
                break;
            case R.id.action_cancel_mark:
                setResult(Activity.RESULT_CANCELED);
                finish();
                consumed = true;
                break;
        }
        return consumed || super.onOptionsItemSelected(item);
    }

    public void onSetTele(View view) {
        mMark = 0f;
        updateMarkFocus();
    }

    public void onSetWide(View view) {
        mMark = 1f;
        updateMarkFocus();
    }

    public void onSetCamera(View view) {
        mMark = mCameraValue;
        updateMarkFocus();
    }

    public void onSync(View view) {
        if (mCameraController != null) {
            mCameraController.syncFocus();
        }
    }

    public void onAutoFocus(View view) {
        if (mCameraController != null) {
            mCameraController.autoFocus();
        }
    }

    private void updateMarkFocus() {
        mMarkBar.setProgress((int) (mMarkBar.getMax() * mMark));
        mMarkBar.postInvalidate();
    }

}
