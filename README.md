# Fokoid
A follow-focus app for Android.
Fokoid is licensed under the [GPLv3](COPYING.md "Full license text").
Additional information is available on the [Fokoid Wiki](https://bitbucket.org/jimknopf/fokoid/wiki "Fokoid wiki").